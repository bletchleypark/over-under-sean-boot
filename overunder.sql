-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: overundertest
-- ------------------------------------------------------
-- Server version	10.3.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application_user`
--

DROP TABLE IF EXISTS `application_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_user` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_6c0v0rco93sykgyetukfmkkod` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_user`
--

LOCK TABLES `application_user` WRITE;
/*!40000 ALTER TABLE `application_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `application_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bucket`
--

DROP TABLE IF EXISTS `bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bucket` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `high_hours` int(11) DEFAULT NULL,
  `low_hours` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `target_hours` int(11) DEFAULT NULL,
  `time_interval` varchar(255) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_he0xrer6rh4dgaalutt7prhbm` (`name`),
  KEY `FKb1dm4mfedwoigh3b9r3bk67e` (`project_id`),
  CONSTRAINT `FKb1dm4mfedwoigh3b9r3bk67e` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bucket`
--

LOCK TABLES `bucket` WRITE;
/*!40000 ALTER TABLE `bucket` DISABLE KEYS */;
INSERT INTO `bucket` VALUES (93,'anonymousUser','2018-09-24 05:05:08','anonymousUser','2018-09-24 05:05:08','2017-08-22 20:00:00',10,-10,'name','2017-08-22 20:00:00',495,'MONTHLY',2),(95,'anonymousUser','2018-09-24 05:05:59','anonymousUser','2018-09-24 05:05:59','2019-08-22 20:00:00',20,-20,'name2','2017-08-22 20:00:00',495,'QUARTER',2);
/*!40000 ALTER TABLE `bucket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `open_air_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','Beacon Health','Beacon Health');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (96),(96),(96),(96),(96),(96);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `open_air_name` varchar(255) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8nw995uro0115f1go0dmrtn2d` (`client_id`),
  CONSTRAINT `FK8nw995uro0115f1go0dmrtn2d` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (2,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','Provider Data Connects','Provider Data Connects',1);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker`
--

DROP TABLE IF EXISTS `worker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaljx9cf62s9g8u5bivyioukf3` (`project_id`),
  CONSTRAINT `FKaljx9cf62s9g8u5bivyioukf3` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker`
--

LOCK TABLES `worker` WRITE;
/*!40000 ALTER TABLE `worker` DISABLE KEYS */;
INSERT INTO `worker` VALUES (3,NULL,NULL,'anonymousUser',NULL,'Hornsby, Kaleb',2),(4,NULL,NULL,NULL,NULL,'Sturgis, Charlotte',2);
/*!40000 ALTER TABLE `worker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_bucket`
--

DROP TABLE IF EXISTS `worker_bucket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_bucket` (
  `worker_id` bigint(20) NOT NULL,
  `bucket_id` bigint(20) NOT NULL,
  PRIMARY KEY (`worker_id`,`bucket_id`),
  KEY `FK3gcsjhohraagy7pacc4xvam8p` (`bucket_id`),
  CONSTRAINT `FK3gcsjhohraagy7pacc4xvam8p` FOREIGN KEY (`bucket_id`) REFERENCES `bucket` (`id`),
  CONSTRAINT `FKdax18wpikxt7688cca7iavrp0` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_bucket`
--

LOCK TABLES `worker_bucket` WRITE;
/*!40000 ALTER TABLE `worker_bucket` DISABLE KEYS */;
INSERT INTO `worker_bucket` VALUES (3,93);
/*!40000 ALTER TABLE `worker_bucket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_entry`
--

DROP TABLE IF EXISTS `worker_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `worker_entry` (
  `id` bigint(20) NOT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `minutes` int(11) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `pto` bit(1) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `time_type` varchar(255) DEFAULT NULL,
  `worker_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKirvhgx1n5frujom1kat9eko19` (`worker_id`),
  CONSTRAINT `FKirvhgx1n5frujom1kat9eko19` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_entry`
--

LOCK TABLES `worker_entry` WRITE;
/*!40000 ALTER TABLE `worker_entry` DISABLE KEYS */;
INSERT INTO `worker_entry` VALUES (5,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-01 00:00:00',120,'Sprint planning','\0','APPROVED','Meeting',3),(6,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-01 00:00:00',30,'daptiv training','\0','APPROVED','Training',3),(7,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-01 00:00:00',330,'entity creation','\0','APPROVED','Development',3),(8,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-02 00:00:00',30,'Daily standup w/ client','\0','APPROVED','Meeting',3),(9,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-02 00:00:00',450,'Developing provider entry','\0','APPROVED','Development',3),(10,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-03 00:00:00',30,'Standup','\0','APPROVED','Meeting',3),(11,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-03 00:00:00',450,'Creating PMDM data models and views and controllers','\0','APPROVED','Development',3),(12,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-06 00:00:00',150,'completed New Hire 2018 Security Awareness Training','\0','APPROVED','Training',3),(13,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-06 00:00:00',30,'Daily Standup','\0','APPROVED','Meeting',3),(14,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-06 00:00:00',300,'Developing PMDM provider and location data models','\0','APPROVED','Development',3),(15,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-07 00:00:00',30,'Daily standup','\0','APPROVED','Meeting',3),(16,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-07 00:00:00',390,'Developing PMDM location contacts editing','\0','APPROVED','Development',3),(17,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-08 00:00:00',90,'Daily standup and meeting to discuss database','\0','APPROVED','Meeting',3),(18,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-08 00:00:00',390,'Updating data models','\0','APPROVED','Development',3),(19,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-09 00:00:00',60,'Daily standup and Jira process overview','\0','APPROVED','Meeting',3),(20,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-09 00:00:00',420,'Creating provider location contacts views','\0','APPROVED','Development',3),(21,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-10 00:00:00',90,'Daily standup and sprint retrospective','\0','APPROVED','Meeting',3),(22,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-10 00:00:00',390,'Finishing up providers and locations and location contacts for sprint review','\0','APPROVED','Development',3),(23,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-13 00:00:00',360,'','\0','APPROVED','Development',3),(24,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-14 00:00:00',420,'','\0','APPROVED','Development',3),(25,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-15 00:00:00',450,'','\0','APPROVED','Development',3),(26,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-16 00:00:00',450,'','\0','APPROVED','Development',3),(27,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-17 00:00:00',390,'','\0','APPROVED','Development',3),(28,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-13 00:00:00',60,'Sprint planning','\0','APPROVED','Meeting',3),(29,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-14 00:00:00',60,'standup/bi-weekly update','\0','APPROVED','Meeting',3),(30,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-15 00:00:00',30,'','\0','APPROVED','Meeting',3),(31,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-16 00:00:00',30,'','\0','APPROVED','Meeting',3),(32,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-17 00:00:00',90,'standup/retrospective','\0','APPROVED','Meeting',3),(33,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-13 00:00:00',60,'BHO product overview','\0','APPROVED','Meeting',3),(34,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-20 00:00:00',60,'Sprint Planning','\0','APPROVED','Meeting',3),(35,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-21 00:00:00',30,'Standup','\0','APPROVED','Meeting',3),(36,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-22 00:00:00',60,'Standup/PMDM User Role Data Meeting','\0','APPROVED','Meeting',3),(37,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-23 00:00:00',30,'Standup','\0','APPROVED','Meeting',3),(38,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-24 00:00:00',90,'Standup/NetworkConnect Overview','\0','APPROVED','Meeting',3),(39,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-20 00:00:00',420,'','\0','APPROVED','Development',3),(40,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-21 00:00:00',450,'','\0','APPROVED','Development',3),(41,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-22 00:00:00',120,'','\0','APPROVED','Development',3),(42,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-22 00:00:00',300,'Network Connects learning','\0','APPROVED','Requirements Gathering',3),(43,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-23 00:00:00',450,'Network Connects learning','\0','APPROVED','Requirements Gathering',3),(44,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-24 00:00:00',390,'Network Connects learning','\0','APPROVED','Requirements Gathering',3),(45,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-27 00:00:00',180,'','\0','APPROVED','Meeting',3),(46,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-28 00:00:00',60,'','\0','APPROVED','Meeting',3),(47,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-29 00:00:00',30,'','\0','APPROVED','Meeting',3),(48,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-30 00:00:00',120,'','\0','APPROVED','Meeting',3),(49,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-31 00:00:00',120,'','\0','APPROVED','Meeting',3),(50,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-27 00:00:00',300,'','\0','APPROVED','Requirements Gathering',3),(51,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-28 00:00:00',420,'','\0','APPROVED','Requirements Gathering',3),(52,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-29 00:00:00',450,'','\0','APPROVED','Requirements Gathering',3),(53,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-30 00:00:00',360,'','\0','APPROVED','Requirements Gathering',3),(54,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-08-31 00:00:00',360,'','\0','APPROVED','Requirements Gathering',3),(55,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-04 00:00:00',120,'Sprint Planning','\0','SUBMITTED','Meeting',3),(56,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-05 00:00:00',30,'Daily Standup','\0','SUBMITTED','Meeting',3),(57,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-06 00:00:00',30,'Daily Standup','\0','SUBMITTED','Meeting',3),(58,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-07 00:00:00',90,'Daily Standup and Data Mapping Meeting','\0','SUBMITTED','Meeting',3),(59,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-04 00:00:00',360,'','\0','SUBMITTED','Requirements Gathering',3),(60,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-05 00:00:00',450,'Creating FlexCare Data Entities','\0','SUBMITTED','Development',3),(61,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-06 00:00:00',450,'Creating FlexCare Data Entities','\0','SUBMITTED','Development',3),(62,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-07 00:00:00',390,'Creating FlexCare Data Entities','\0','SUBMITTED','Development',3),(63,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-10 00:00:00',30,'Daily Standup','\0','OPEN','Meeting',3),(64,'anonymousUser','2018-09-24 05:04:42','anonymousUser','2018-09-24 05:04:42','2018-09-11 00:00:00',30,'Daily Standup','\0','OPEN','Meeting',3),(65,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-10 00:00:00',450,'','\0','OPEN','Development',3),(66,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-11 00:00:00',450,'','\0','OPEN','Development',3),(67,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-06 00:00:00',480,'Daily Stand-up, Client Meetings, Business Requirements gathering SOW UUI, PMDM & EDI.','\0','APPROVED','Requirements Gathering',4),(68,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-07 00:00:00',480,'Daily Stand-up, Client Meetings, Business Requirements gathering SOW UUI, PMDM & EDI.','\0','APPROVED','Requirements Gathering',4),(69,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-08 00:00:00',600,'Daily Stand-up, Client Meetings, Business Requirements gathering SOW UUI, PMDM & EDI.','\0','APPROVED','Requirements Gathering',4),(70,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-09 00:00:00',600,'Daily Stand-up, Client Meetings, Business Requirements gathering SOW UUI, PMDM & EDI.','\0','APPROVED','Requirements Gathering',4),(71,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-10 00:00:00',480,'Daily Stand-up, Client Meetings, Business Requirements gathering SOW UUI, PMDM & EDI.','\0','APPROVED','Requirements Gathering',4),(72,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-13 00:00:00',450,'Beacon Health meetings, user stories evaluation, FC 360 & NWC Review.','\0','APPROVED','Requirements Gathering',4),(73,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-14 00:00:00',450,'Beacon Health meetings, user stories evaluation, FC 360 & NWC Review.','\0','APPROVED','Requirements Gathering',4),(74,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-15 00:00:00',540,'Beacon Health meetings, user stories evaluation, FC 360 & NWC Review.','\0','APPROVED','Requirements Gathering',4),(75,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-16 00:00:00',570,'Beacon Health meetings, user stories evaluation, FC 360 & NWC Review.','\0','APPROVED','Requirements Gathering',4),(76,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-17 00:00:00',480,'Beacon Health meetings, user stories evaluation, FC 360 & NWC Review.','\0','APPROVED','Requirements Gathering',4),(77,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-20 00:00:00',510,'Beacon Health SCRUM Daily Meetings, Requirement Gathering, Client Meetings, Conference Calls and Applications Analysis.','\0','APPROVED','Requirements Gathering',4),(78,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-21 00:00:00',510,'Beacon Health SCRUM Daily Meetings, Requirement Gathering, Client Meetings, Conference Calls and Applications Analysis.','\0','APPROVED','Requirements Gathering',4),(79,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-22 00:00:00',540,'Beacon Health SCRUM Daily Meetings, Requirement Gathering, Client Meetings, Conference Calls and Applications Analysis.','\0','APPROVED','Requirements Gathering',4),(80,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-23 00:00:00',540,'Beacon Health SCRUM Daily Meetings, Requirement Gathering, Client Meetings, Conference Calls and Applications Analysis.','\0','APPROVED','Requirements Gathering',4),(81,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-24 00:00:00',480,'Beacon Health SCRUM Daily Meetings, Requirement Gathering, Client Meetings, Conference Calls and Applications Analysis.','\0','APPROVED','Requirements Gathering',4),(82,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-27 00:00:00',420,'Gathered requirements, meetings, research & JIRA Input.','\0','APPROVED','Requirements Gathering',4),(83,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-28 00:00:00',540,'Gathered requirements, meetings, research & JIRA Input.','\0','APPROVED','Requirements Gathering',4),(84,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-29 00:00:00',570,'Gathered requirements, meetings, research & JIRA Input.','\0','APPROVED','Requirements Gathering',4),(85,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-30 00:00:00',510,'Gathered requirements, meetings, research & JIRA Input.','\0','APPROVED','Requirements Gathering',4),(86,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-08-31 00:00:00',450,'Gathered requirements, meetings, research & JIRA Input.','\0','APPROVED','Requirements Gathering',4),(87,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-04 00:00:00',510,'','\0','APPROVED','Requirements Gathering',4),(88,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-05 00:00:00',540,'','\0','APPROVED','Requirements Gathering',4),(89,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-06 00:00:00',570,'','\0','APPROVED','Requirements Gathering',4),(90,'anonymousUser','2018-09-24 05:04:43','anonymousUser','2018-09-24 05:04:43','2018-09-07 00:00:00',480,'','\0','APPROVED','Requirements Gathering',4),(91,'anonymousUser','2018-09-24 05:04:52','anonymousUser','2018-09-24 05:04:52','2018-11-26 00:00:00',480,'Vacation','','APPROVED','Time Off',4),(92,'anonymousUser','2018-09-24 05:04:52','anonymousUser','2018-09-24 05:04:52','2018-11-27 00:00:00',480,'Vacation','','APPROVED','Time Off',4);
/*!40000 ALTER TABLE `worker_entry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-24  9:25:16
