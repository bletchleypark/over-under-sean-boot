#!/bin/bash
curl -H "Content-Type: multipart/form-data" \
 -H "Authorization: Bearer $2" \
 -F "file=@$1" \
 -F "filename=Beaconfile.csv" \
/api/v1/uploadwork
