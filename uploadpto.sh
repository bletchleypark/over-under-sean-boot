#!/bin/bash
curl -H "Content-Type: multipart/form-data" \
 -H "Authorization: Bearer $2" \
 -F "file=@$1" \
 -F "filename=Time_Off_Requests__Q4_report.csv" \
http://localhost:8080/api/v1/uploadpto
