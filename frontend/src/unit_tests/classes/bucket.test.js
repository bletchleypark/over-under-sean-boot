import { Bucket } from '../../client/classes/bucket'

test('bucket object initializes', () => {
    expect(new Bucket()).toBeInstanceOf(Bucket);
});