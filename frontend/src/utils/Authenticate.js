export default async token => {
  let authorized;
  try {
    const response = await fetch(`http://localhost:8080/api/v1/authorize`, {
      headers: { Authorization: `Bearer ${token}` },
    });
    authorized = await response.json();
    console.log('response');
  } catch (err) {
    return {
      error: err,
    }
  }
  return authorized;

};
