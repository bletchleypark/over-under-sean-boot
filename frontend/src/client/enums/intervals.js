var BillingIntervals = Object.freeze({
    "None": null,
    "Monthly": 1,
    "Quarter": 3,
    "Semi-Annual": 6,
    "Annual": 12
});

export default BillingIntervals;