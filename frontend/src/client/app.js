import React, { Component } from 'react';
import { Navbar } from './components';
import Routes from './routes';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faStroopwafel, faKey, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

library.add(faStroopwafel, faKey, faSignOutAlt)

class App extends Component {
  state = {
    userAuthenticated: false,
  };
  login = () => this.setState({ userAuthenticated: true });
  render() {
    return (
      <div id='root'>
        <Navbar userAuthenticated={this.state.userAuthenticated} />
        <Routes login={this.login} />
      </div>
    );
  }
}

export default App;