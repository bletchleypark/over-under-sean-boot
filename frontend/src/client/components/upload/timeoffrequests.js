import React, { Component } from 'react';
import authenticate from '../../../utils/Authenticate';
import Spinner from '../../components/Spinner';
export default class UploadTimeoff extends Component {
  state = {
    file: null,
    filename: null,
    error: false,
    success: false,
    isLoading: true,
    isUploading: false,
  };

  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          this.setState({ isLoading: false });
        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };

  handleUpload = e => {
    e.preventDefault();
    this.setState(() => ({ isUploading: true }));
    let { file, filename } = this.state;

    const data = new FormData();
    data.append('file', file);
    data.append('filename', filename);

    fetch('http://localhost:8080/api/v1/uploadpto', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      method: 'POST',
      body: data
    }).then(response => response.json())
      .then(({ success, error }) => {
        this.setState(() => ({
          success,
          error,
          isUploading: false,
        }));
      }).catch((error) => {
        console.log(`Error: ${error}`);
        this.setState(() => ({
          error: true,
          success: false,
          isUploading: false,
        }));
      })
  };

  handleAttach = e => {
    let file = e.target.files[0];
    let name = file.name;

    this.setState({ file: file, filename: name });
  }

  render() {
    const {
      filename,
      error,
      success,
      isLoading,
      isUploading,
    } = this.state;
    return (
      isLoading ? <Spinner /> :
        <div className="container">
          <div className="row">
            <div className="col-sm-1"></div>
            <div className="col-sm-10">
              <div className="grey-box format-box">
                <h1>Upload Time-Off Request CSV</h1>
                <div className="input-group mb-3">
                  <div className="custom-file">
                    <input type="file" className="custom-file-input" id="fileUpload" aria-describedby="inputGroupFileAddon01" onChange={this.handleAttach} />
                    <label className="custom-file-label" htmlFor="fileUpload" id="filename">{filename ? filename : 'Choose file'}</label>
                  </div>
                </div>
                {error ? <div className='alert alert-danger mr-3'>An error occured uploading the file.</div> : null}
                {success ? <div className='alert alert-success mr-3'>File successfully uploaded!</div> : null}
                {!isUploading &&
                  <button className="btn btn-primary" onClick={this.handleUpload}>
                    Upload
                  </button>
                }
                {isUploading &&
                  <div style={{ display: 'flex' }}>
                    <button className="btn btn-primary" style={{ height: 50 }} onClick={this.handleUpload}>
                      Upload
                  </button>
                    <span style={{ marginLeft: '25%' }}>
                      <Spinner size={100} />
                    </span>
                  </div>
                }
              </div>
            </div>
            <div className="col-sm-1"></div>
          </div>
        </div>
    );
  }
}

