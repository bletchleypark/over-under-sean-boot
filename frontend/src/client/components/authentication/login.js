import React, { Component } from 'react';

export default class Login extends Component {
  state = {
    username: '',
    password: '',
    error: false,
  };
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  }

  authLogin = e => {
    e.preventDefault();
    const { username, password } = this.state;
    const data = JSON.stringify({ username, password });
    fetch('http://localhost:8080/api/v1/auth/login', {
      headers: {
        "Content-Type": "application/json"
      },
      body: data,
      method: 'POST'
    })
      .then(response => response.json())
      .then(({ token }) => {
        if (token) {
          this.props.login();
          localStorage.setItem('token', token);
          this.props.history.push('/')
        } else {
          this.setState({ error: true })
          alert("Invalid Credentials");
        }
      })
      .catch(error => {
        console.error(error);
      })
  };

  render() {
    const { username, password, error } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-3"></div>
          <div className="col-sm-6">
            <div className="grey-box format-box">
              <h1>Login</h1>
              <form>
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <input
                    className="form-control"
                    name="username"
                    type="text"
                    value={username}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input
                    className="form-control"
                    name="password"
                    type="password"
                    value={password}
                    onChange={this.handleChange}
                  />
                </div>
                {error ? <div className='alert alert-danger mr-3'>Invalid username or password.</div> : null}
              </form>
              <div>
                <button
                  className="btn btn-primary"
                  onClick={this.authLogin}
                >Login</button>
              </div>
            </div>
          </div>
          <div className="col-sm-3"></div>
        </div>
      </div>
    )
  }
}

