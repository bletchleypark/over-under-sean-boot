import React, { Component } from 'react';
export default class Register extends Component {

  state = {
    username: '',
    password: '',
  };

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  authRegister = e => {
    e.preventDefault();
    const { username, password } = this.state;
    const data = JSON.stringify({ username, password });
    fetch('http://localhost:8080/api/v1/users/sign-up', {
      headers: {
        "Content-Type": "application/json"
      },
      body: data,
      method: 'POST'
    })
      .then(response => response.json())
      .then(({ user }) => {
        if (user) {
          alert("success");
        } else {
          this.setState({ error: true })
          alert("Invalid Credentials");
        }
      })
      .catch(error => {
        console.error(error);
      })
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-3"></div>
          <div className="col-sm-6">
            <div className="grey-box format-box">
              <h1>Register</h1>
              <form>
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <input className="form-control" name="username" type="text" onChange={this.handleChange} />
                </div>
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input className="form-control" name="password" type="password" onChange={this.handleChange} />
                </div>
                {this.props.error ? <div className='alert alert-danger mr-3'>Invalid username or password.</div> : null}
                <div>
                  <button className="btn btn-primary" onClick={this.authRegister}>
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-sm-3"></div>
        </div>
      </div>
    );
  }
}


