const loadBucketPolicies = async (clientId, projectId) => {
  try {
    const response = await fetch(`http://localhost:8080/api/v1/buckets/client/${clientId}/project/${projectId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    });
    const json = await response.json();
    const { buckets, error } = json;
    if (buckets) {
      return buckets;
    } else if (error) {
      return null;
    }
  } catch (error) {
    return null;
  }
};

export {
  loadBucketPolicies,
};