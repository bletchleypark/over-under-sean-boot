import React, { Component } from 'react';
import BucketDropdown from '../dropdowns/bucket';
import authenticate from '../../../utils/Authenticate';
import Spinner from '../../components/Spinner';
import { loadBucketPolicies } from './bucketUtils';

export default class BucketPolicies extends Component {

  state = {
    buckets: [],
    selectedBucket: '',
    multiplier: '',
    startDate: '',
    updateSuccess: false,
    errorDate: false,
    isLoading: true,
  };

  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          const buckets = await loadBucketPolicies(this.props.match.params.clientId, this.props.match.params.projectId);
          if (buckets) this.setState(() => ({ buckets }));
          this.setState(() => ({ isLoading: false }));
        } else if (error) {
          this.props.history.push('/login')
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login')
        localStorage.removeItem('token');
      }
    }
  };

  selectBucket = evt => {
    evt.preventDefault();
    const buckets = [...this.state.buckets];
    const selectedBucket = buckets[parseInt(evt.target.value, 10)];
    let noDate = false;
    for (let propName in selectedBucket) {
      if (propName === 'startDate') {
        if (selectedBucket[propName] === null) {
          noDate = true;
        }
      }
    }

    if (noDate) {
      this.setState({
        updateSuccess: false,
        projectId: evt.target.value,
        ...selectedBucket,
        selectedBucket,
        startDate: '',
      });
    } else {
      this.setState({
        updateSuccess: false,
        projectId: evt.target.value,
        ...selectedBucket,
        selectedBucket,
      });
    }
  };

  handleChange = e => {
    const key = e.target.name;
    const value = e.target.value
    if (key === 'startDate') this.setState(() => ({ errorDate: false }));
    this.setState(() => ({ [key]: value }));
  }

  onSubmit = e => {
    e.preventDefault();
    const { startDate, multiplier } = this.state;
    if (!startDate) {
      this.setState({ errorDate: true });
      return;
    }
    let bucket = { ...this.state.selectedBucket };
    bucket.startDate = startDate;
    bucket.multiplier = multiplier;
    const data = JSON.stringify(bucket);
    fetch('http://localhost:8080/api/v1/buckets', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
      },
      body: data,
      method: 'PUT'
    })
      .then(response => response.json())
      .then(async ({ bucket, error }) => {
        if (bucket) {
          const buckets = await loadBucketPolicies(this.props.match.params.clientId, this.props.match.params.projectId);
          if (buckets) {
            this.setState(() => ({
              updateSuccess: true,
              buckets,
              startDate: '',
            }));
          }
        } else if (error) {
          alert(error);
        }
      })
      .catch(error => {
        console.error(error);
      })
  };
  render() {
    const {
      multiplier,
      startDate,
      updateSuccess,
      errorDate,
      isLoading,
    } = this.state;

    return (
      isLoading ? <Spinner /> :
        <div className="container">
          <div className="row">
            <div className="col-sm-10">
              <div className="grey-box format-box">
                <h1 className='text-center'>Update Bucket Polices</h1>
                <form>
                  <div className="form-group">
                    <div>
                      <BucketDropdown
                        buckets={this.state.buckets}
                        selectBucket={this.selectBucket}
                      />
                    </div>
                    <div>
                      <label htmlFor="startDate" className="proj-label">
                        <small>Start Date</small>
                      </label>
                      <input
                        className="form-control"
                        name="startDate"
                        type="date"
                        onChange={this.handleChange}
                        value={startDate}
                      />
                    </div>
                    <div className="form-group row">
                      <label htmlFor="multiplier" className="col-sm-3 col-form-label">
                        Multiplier
                    </label>
                      <div className="col-sm-9">
                        {errorDate && <div style={{ color: 'red' }}>Please input a date</div>}
                        <input
                          type="number"
                          className="form-control"
                          id="multiplier"
                          name="multiplier"
                          onChange={this.handleChange}
                          value={multiplier}
                          step="0.01"
                          required
                        />
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-primary" onClick={this.onSubmit}>
                    Update Bucket Policy
                </button>
                  <br />
                  <br />
                  {updateSuccess && <div className='alert alert-success mr-3'>Bucket Policy Updated Successfully!</div>}
                </form>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

