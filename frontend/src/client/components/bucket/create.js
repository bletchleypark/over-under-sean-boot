import React, { Component } from 'react';
import ClientDropdown from '../dropdowns/client';
import ProjectDropdown from '../dropdowns/project';
import authenticate from '../../../utils/Authenticate';
import Spinner from '../../components/Spinner';
export default class AddInterval extends Component {

  state = {
    clientId: '',
    projectId: '',
    projects: [],
    selectedProject: '',
    startDate: '',
    endDate: '',
    hours: '',
    isLoading: true,
  };

  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          this.setState({ isLoading: false });
        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };

  loadProjects = clientId => {
    fetch(`http://localhost:8080/api/v1/projects/client/${clientId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
    }).then(response => response.json())
      .then(({ projects, error }) => {
        if (projects) {
          this.setState({ projects });
        } else if (error) {
          console.log(error);
        }
      }).catch((error) => {
        console.log(`Error: ${error}`);
      });
  };

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  selectClient = evt => {
    evt.preventDefault();
    let clientId = evt.target.value;
    this.setState({ clientId });
    this.loadProjects(clientId);
  };

  selectProject = evt => {
    evt.preventDefault();
    const selectedProject = this.state.projects.find(project => project.id === parseInt(evt.target.value, 10));
    for (let propName in selectedProject) {
      if (selectedProject[propName] === null) {
        selectedProject[propName] = '';
      }
    };

    this.setState({
      projectId: evt.target.value,
      ...selectedProject,
      selectedProject,
    });
  };

  onSubmit = e => {
    e.preventDefault();

    const {
      startDate,
      endDate,
      hours,
      timeInterval,
      clientId,
      thresholdUnder,
      thresholdOver,
    } = this.state;

    let project = { ...this.state.selectedProject };
    project.startDate = startDate;
    project.endDate = endDate;
    project.timeInterval = timeInterval;
    project.thresholdUnder = thresholdUnder;
    project.thresholdOver = thresholdOver;
    project.hours = hours;

    const data = JSON.stringify(project);
    fetch('http://localhost:8080/api/v1/projects', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json"
      },
      body: data,
      method: 'PUT'
    })
      .then(response => response.json())
      .then(({ project, error }) => {
        if (project) {
          this.props.history.push(`/bucketpolicies/${clientId}/${project.id}`)
        } else if (error) {
          alert(error);
        }
      })
      .catch(error => {
        console.error(error);
      })
  };

  render() {
    const {
      startDate,
      endDate,
      hours,
      thresholdOver,
      thresholdUnder,
      timeInterval,
    } = this.state;
    return (
      this.state.isLoading ? <Spinner /> :
        <div className="container">
          <div className="row">
            <div className="col-sm-10">
              <div className="grey-box format-box">
                <h1 className='text-center'>Update Project Interval</h1>
                <form>
                  <div className="form-group">
                    <div>
                      <ClientDropdown selectClient={this.selectClient} />
                      <ProjectDropdown
                        selectProject={this.selectProject}
                        projects={this.state.projects}
                      />
                    </div>
                    <div>
                      <label htmlFor="startDate" className="proj-label">
                        <small>Start Date</small>
                      </label>
                      <input
                        className="form-control"
                        name="startDate"
                        type="date"
                        onChange={this.handleChange}
                        value={startDate}
                        required
                      />
                    </div>
                    <div>
                      <label htmlFor="endDate" className="proj-label">
                        <small>End Date</small>
                      </label>
                      <input
                        className="form-control"
                        name="endDate"
                        type="date"
                        value={endDate}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group row">
                      <label htmlFor="target_hours" className="col-sm-3 col-form-label">
                        Target Hours
                    </label>
                      <div className="col-sm-9">
                        <input
                          type="number"
                          className="form-control"
                          id="target_hours"
                          placeholder="Ex. 495"
                          name="hours"
                          onChange={this.handleChange}
                          value={hours}
                          required
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="minimumThreshold" className="col-sm-3 col-form-label">
                        Minimum Threshold
                    </label>
                      <div className="col-sm-3">
                        <input
                          type="number"
                          className="form-control"
                          id="minimumThreshold"
                          name="thresholdUnder"
                          value={thresholdUnder}
                          placeholder="Ex. 0.10"
                          onChange={this.handleChange}
                          required
                        />
                      </div>
                      <label htmlFor="maximumThreshold" className="col-sm-3 col-form-label">Maximum Threshold</label>
                      <div className="col-sm-3">
                        <input
                          type="number"
                          className="form-control"
                          id="maximumThreshold"
                          name="thresholdOver"
                          placeholder="Ex. 0.10"
                          value={thresholdOver}
                          onChange={this.handleChange}
                          required
                        />
                      </div>
                    </div>
                    <div className="drop-down" >
                      <label htmlFor="interval" className="proj-label">Billing Interval</label>
                      <select
                        name="timeInterval"
                        className="col-sm-6 form-control"
                        onChange={this.handleChange}
                        value={timeInterval}
                      >
                        <option value={"Choose Bill Interval"}>Choose bill interval</option>
                        <option value={"MONTH"}>Monthly</option>
                        <option value={"QUARTER"}>Quarter</option>
                        <option value={"SEMIANNUAL"}>Semi-Annual</option>
                        <option value={"ANNUAL"}>Annual</option>
                      </select>
                    </div>
                  </div>
                  <button className="btn btn-primary" onClick={this.onSubmit}>
                    Update Project
                </button>
                  <br />
                  <br />
                </form>
              </div>
            </div>
          </div>
        </div>
    )
  }
}
