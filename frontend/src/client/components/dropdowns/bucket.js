import React from 'react';

export default ({ buckets, selectBucket }) =>
  <div class="form-group">
    <select className="btn btn-dark" onClick={selectBucket} required>
      <option value="" selected="selected">
        Select Bucket Policy
      </option>
      {!!buckets.length &&
        buckets.map((bucket, index) =>
          <option
            value={index}
            key={`bucket${bucket.bucketPolicyIdentity.client.id}${bucket.bucketPolicyIdentity.project.id}${bucket.bucketPolicyIdentity.worker.id}`}
          >
            {bucket.name.split('_')[0]}
          </option>
        )}
    </select>
  </div>
