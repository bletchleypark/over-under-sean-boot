import React, { Component } from 'react';

export default class ProjectDropdown extends Component {
  render() {
    const { projects } = this.props;
    return (
      <div className="form-group">
        <select className="btn btn-dark" onChange={this.props.selectProject} required>
          <option value="">
            Select Project
          </option>
          {!!projects.length &&
            projects.map(client =>
              <option
                value={client.id}
                key={`client${client.id}`}
              >
                {client.name}
              </option>
            )}
        </select>
      </div>
    )
  }
}

