import React, { Component } from 'react';

export default class ClientDropdownTwo extends Component {
  state = {
    clients: [],
  };
  componentDidMount() {
    this.loadClients();
  };

  loadClients = () => {
    fetch('http://localhost:8080/api/v1/clients', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
    }).then(response => response.json())
      .then(({ clients, error }) => {
        if (clients) {
          this.setState({ clients });
        } else if (error) {
          console.log(error);
        }
      }).catch((error) => {
        console.log(`Error: ${error}`);
      });
  };
  render() {
    const { clients } = this.state;
    return (
      <div class="form-group">
        <select className="btn btn-dark" onChange={this.props.selectClient} required>
          <option value="" selected="selected">Select Client</option>
          {!!clients.length &&
            clients.map(client => {
              return (
                <option
                  value={`${client.name}_${client.id}`}
                  key={`client${client.id}`}
                >{client.name}</option>
              )
            })}
        </select>
      </div>
    )
  }
}
