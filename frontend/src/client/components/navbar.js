import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Brand from "./brand";
import authenticate from '../../utils/Authenticate';

export default class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: props.userAuthenticated !== undefined ? props.userAuthenticated : false,
      isClientOpen: false,
      isDataOpen: false,
    };
  }
  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          this.setState({ isLoggedIn });
        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };
  logout = () => {
    localStorage.removeItem('token');
    fetch(`http://localhost:8080/api/v1/auth/logout`)
  };
  toggleClient = () => {
    this.setState({ isClientOpen: !this.state.isClientOpen });
  };
  toggleData = () => {
    console.log("togg")
    this.setState({ isDataOpen: !this.state.isDataOpen });
  };
  render() {
    const {
      isLoggedIn,
      isClientOpen,
      isDataOpen,
    } = this.state;
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand">
          <Brand />
        </Link>
        <div className="navbar-collapse">
          {!isLoggedIn && !this.props.userAuthenticated ? (
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to="/login">
                  <FontAwesomeIcon icon="key" /> Login
                  </Link>
              </li>
              <li className="nav-item">
                <Link to="/register">
                  Register
                </Link>
              </li>
            </ul>
          ) : (
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to="/" className="nav-link">Home</Link>
                </li>

                <li onClick={this.toggleClient}>
                  <div className="dropdown">
                    <a
                      className="nav-link dropdown-toggle"
                      id="dropdownMenuButton"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      Client
                    </a>
                    {isClientOpen &&
                      <div aria-labelledby="dropdownMenuButton">
                        <Link to="/project/update" className="dropdown-item">
                          Update Project
                        </Link>
                        <Link to="/projects" className="dropdown-item">
                          Over/Under
                    </Link>
                      </div>
                    }
                  </div>
                </li>

                <li onClick={this.toggleData}>
                  <a className="nav-link dropdown-toggle" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
                    Data
                    </a>
                  {isDataOpen &&
                    <div aria-labelledby="dropdownMenuButton">
                      <Link to="/upload/timesheet" className="dropdown-item">
                        Upload Timesheet CSV
                        </Link>
                      <Link to="/upload/timeoff" className="dropdown-item">
                        Upload Time Off Request CSV
                        </Link>
                    </div>
                  }
                </li>

                <li className="nav-item">
                  <Link to="/login" onClick={this.logout} className="nav-link">
                    <FontAwesomeIcon icon="sign-out-alt" /> Logout
                </Link>
                </li>
              </ul>
            )}
        </div>
      </nav>
    );
  }
}
