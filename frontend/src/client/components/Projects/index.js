import React, { Component } from 'react';
import ClientDropdownTwo from '../dropdowns/ClientDropdownTwo';
import ProjectDropdown from '../dropdowns/project';
import Spinner from '../../components/Spinner';
import authenticate from '../../../utils/Authenticate';
import './index.css';


export default class Projects extends Component {

  state = {
    workers: [],
    projects: [],
    selectedProject: '',
    startDate: '',
    endDate: '',
    hours: '',
    thresholdOver: '',
    thresholdUnder: '',
    isLoading: true,
  };

  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          this.setState({ isLoading: false });
        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };

  loadProjects = clientId => {
    fetch(`http://localhost:8080/api/v1/projects/client/${clientId}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
    }).then(response => response.json())
      .then(({ projects, error }) => {
        if (projects) {
          this.setState({ projects });
        } else if (error) {
          console.log(error);
        }
      }).catch((error) => {
        console.log(`Error: ${error}`);
      });
  };
  getColor = (isOver, isUnder, isWithinThreshold) => {
    if (isOver) {
      return '';
    } else if (isUnder) {
      return 'blue';
    } else if (isWithinThreshold) {
      return '#6dd641 ';
    }
  }

  selectClient = async evt => {
    evt.preventDefault();
    let client = evt.target.value.split('_');
    let clientName = client[0];
    let clientId = client[1];
    this.setState({ clientName, clientId });
    this.loadProjects(clientId);
  };

  selectProject = evt => {
    evt.preventDefault();
    const selectedProject = this.state.projects.find(project => project.id === parseInt(evt.target.value, 10));
    this.setState({
      projectId: evt.target.value,
      ...selectedProject,
      selectedProject,
    }, () => {
      this.props.history.push(`/projectedhours/client/${this.state.clientName}/project/${this.state.selectedProject.name}`);
    });
  };

  render() {
    const {
      workers,
      isLoading,
      projects,
    } = this.state;

    return (
      isLoading ? <Spinner /> :
        <div>
          <div className="container">
            <div className="row">
              <div className="col-sm-2"></div>
              <div className="col-sm-8">
                <div className="grey-box format-box">
                  <div className="projects">
                    <h1 className="text-center">Projects</h1>
                    <div>
                      <ClientDropdownTwo selectClient={this.selectClient} />
                      <ProjectDropdown
                        selectProject={this.selectProject}
                        projects={projects}
                      />
                    </div>
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Hours</th>
                        </tr>
                      </thead>
                      <tbody className="white-box">
                        {!!workers.length &&
                          <React.Fragment>
                            {workers.map(({
                              amount,
                              worker,
                              isOver,
                              isUnder,
                              isWithinThreshold,
                            }) =>
                              <tr>
                                <td>{worker}</td>
                                <td style={{
                                  background: this.getColor(isOver, isUnder, isWithinThreshold),
                                }}>
                                  <span style={{ float: 'left', marginLeft: 16 }}>
                                    {amount > 0 ? 'over target by ' : 'under target by '}
                                  </span>
                                  <span style={{ float: 'right', marginRight: 32 }}>
                                    {amount}
                                  </span></td>
                              </tr>
                            )}
                          </React.Fragment>
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div className="col-sm-2"></div>
            </div>
          </div>
        </div>
    );
  }

}

