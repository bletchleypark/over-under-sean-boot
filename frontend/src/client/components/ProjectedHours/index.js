import React, { Component } from 'react';
import { loadWorkers } from './projectUtils';
import Spinner from '../../components/Spinner';
import authenticate from '../../../utils/Authenticate';
import './index.css';


export default class Projects extends Component {

  state = {
    workers: [],
    projects: [],
    selectedProject: '',
    startDate: '',
    endDate: '',
    hours: '',
    thresholdOver: '',
    thresholdUnder: '',
    isLoading: true,
  };

  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          const workers = await loadWorkers(this.props.match.params.clientName, this.props.match.params.projectName);
          if (workers) {
            this.setState(() => ({
              isLoading: false,
              workers
            }));
          } else {
            this.setState({ isLoading: false });
          }

        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };

  getColor = (isOver, isUnder, isWithinThreshold, actionItem) => {
    if (isOver) {
      return '';
    } else if (isUnder) {
      return '#e27c7c';
    } else if (isWithinThreshold && !actionItem) {
      return '#6dd641';
    } else if (isWithinThreshold && actionItem) {
      return '#efef84';
    }
  }

  render() {
    const {
      workers,
      isLoading,
    } = this.state;

    return (
      isLoading ? <Spinner /> :
        <div>
          <div className="container">
            <div className="row">
              <div className="col-sm-2"></div>
              <div className="col-sm-8">
                <div className="grey-box format-box">
                  <div className="projects">
                    <h1 className="text-center">Projects</h1>
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Hours</th>
                        </tr>
                      </thead>
                      <tbody className="white-box">
                        {!!workers.length &&
                          <React.Fragment>
                            {workers.map(({
                              amount,
                              worker,
                              isOver,
                              isUnder,
                              isWithinThreshold,
                              actionItem
                            }, index) =>
                              <tr key={`overunder${index}`}>
                                <td>{worker}</td>
                                <td style={{
                                  background: this.getColor(isOver, isUnder, isWithinThreshold, actionItem),
                                }}>
                                  {isUnder &&
                                    <React.Fragment>
                                      <span style={{ float: 'left', marginLeft: 16 }}>
                                        under target by:
                                      </span>
                                      <span style={{ float: 'right', marginRight: 32 }}>
                                        {amount}
                                      </span>
                                    </React.Fragment>
                                  }
                                  {(actionItem && isUnder) &&
                                    <React.Fragment>
                                      <br />
                                      <span style={{ float: 'left', marginLeft: 16 }}>
                                        action item:
                                      </span>
                                      <span style={{ float: 'right', marginRight: 32 }}>
                                        {actionItem}
                                      </span>
                                    </React.Fragment>
                                  }
                                  {isOver &&
                                    <React.Fragment>
                                      <span style={{ float: 'left', marginLeft: 16 }}>
                                        over target by:
                                      </span>
                                      <span style={{ float: 'right', marginRight: 32 }}>
                                        {amount}
                                      </span>
                                    </React.Fragment>
                                  }
                                  {isWithinThreshold &&
                                    <React.Fragment>
                                      <span style={{ float: 'left', marginLeft: 16 }}>
                                        {amount < 0 ? 'under target by: ' : 'over target by'}
                                      </span>
                                      <span style={{ float: 'right', marginRight: 32 }}>
                                        {amount}
                                      </span>
                                    </React.Fragment>
                                  }
                                  {(isWithinThreshold && actionItem) &&
                                    <React.Fragment>
                                      <br />
                                      <span style={{ float: 'left', marginLeft: 16 }}>
                                        action item:
                                      </span>
                                      <span style={{ float: 'right', marginRight: 32 }}>
                                        {actionItem}
                                      </span>
                                    </React.Fragment>
                                  }
                                </td>
                              </tr>
                            )}
                          </React.Fragment>
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div className="col-sm-2"></div>
            </div>
          </div>
        </div>
    );
  }

}
