const loadWorkers = async (client, project) => {
  try {
    const response = await fetch(`http://localhost:8080/api/v1/projectedhours/client/${client}/project/${project}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    });
    const json = await response.json();
    const { workers, error } = json;
    if (workers) {
      return workers;
    } else if (error) {
      return null;
    }
  } catch (error) {
    return null;
  }
};


export {
  loadWorkers,
};