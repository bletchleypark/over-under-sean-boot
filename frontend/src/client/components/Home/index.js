import React, { Component } from 'react';
import './index.css';
import authenticate from '../../../utils/Authenticate';
import Spinner from '../../components/Spinner';


export default class Home extends Component {
  state = {
    isLoading: true,
  };
  async componentDidMount() {
    let authorized;
    try {
      authorized = await authenticate(localStorage['token']);
    } finally {
      if (authorized !== undefined) {
        const { isLoggedIn, error } = authorized;
        if (isLoggedIn) {
          this.setState({ isLoading: false });
        } else if (error) {
          this.props.history.push('/login');
          localStorage.removeItem('token');
        }
      } else {
        this.props.history.push('/login');
        localStorage.removeItem('token');
      }
    }
  };
  render() {
    return (
      this.state.isLoading ? <Spinner /> :
        <div className="home">
          <div className="container-home">
            Home
        </div>
        </div>
    )
  }
}
