import React from "react";
import { Route, Switch } from "react-router";
import {
  Login,
  Home,
  UploadTimesheet,
  UploadTimeoff,
  AddInterval,
  Register,
  Projects,
  BucketPolicies,
} from "./components";
import ProjectedHours from './components/ProjectedHours'

const Routes = ({ login }) =>
  <Switch>
    <Route
      exact path="/"
      render={props => <Home {...props} />}
    />
    <Route
      path="/register"
      render={props => <Register {...props} />}
    />
    <Route
      path="/login"
      render={props => <Login login={login} {...props} />}
    />
    <Route
      path="/upload/timesheet"
      render={props => <UploadTimesheet {...props} />}
    />
    <Route
      path="/upload/timeoff"
      render={props => <UploadTimeoff {...props} />}
    />
    <Route
      path="/project/update"
      render={props => <AddInterval {...props} />}
    />
    <Route
      path="/bucketpolicies/:clientId/:projectId"
      render={props => <BucketPolicies {...props} />}
    />
    <Route
      path="/projectedhours/client/:clientName/project/:projectName"
      render={props => <ProjectedHours {...props} />}
    />
    <Route
      path="/projects"
      render={props => <Projects {...props} />}
    />
  </Switch>

export default Routes; 
