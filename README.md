# Over Under App 
Internal project at Rural Sourcing to allow project managers to more efficiently  track hours on projects that are billed by the hour.

**Dependencies:** 

- *`Java Development Kit` available in the portal manager+*
- *`Node` available in the portal manager+*
- [`Maven`](https://maven.apache.org/download.cgi)
- [`MariaDB`](https://downloads.mariadb.org/mariadb/10.3.9/#os_group=windows)

## Steps to set up MariaDB
![picture](https://s3.us-east-2.amazonaws.com/suggestify/mbd.png)

##### Extract the zip folder to **C:\Users\your.name\mariadb** 

## Get the Project from Source Control

```sh
git clone git@bitbucket.org:bletchleypark/over-under-sean-boot.git
cd frontend
npm install
```

### Add Development Database
#### 1. Navigate to the folder you installed MariaDB
![picture](https://s3.us-east-2.amazonaws.com/suggestify/dbpath.png)

#### 2. Copy the path where you installed MariaDB   
![picture](https://s3.us-east-2.amazonaws.com/suggestify/path.png)
#### 3. open cmd prompt
  
```sh
cd C:\Users\austi\mariadb

cd mariadb-10.3.9-winx64

cd bin

./mysqld 
```

#### 4. Leave cmd prompt running, minimize it and open another cmd prompt window
#### 5. add development database

```cmd
cd C:\Users\austi\Documents\over-under-sean-boot
```
```sql
mysql --user root --password < overunder.sql
Enter Password: *press enter*
```
#### 6. confirm data was uploaded correctly
```sql
mysql -uroot
use overundertest;
show tables;
select * from workers;
```