package com.ruralsourcing.bp.overunder.enums;

public enum TimeWorkedRecord {
    /*
    1: Timesheet name
2: Client
3: Project
4: Service
5: Time type
6: User
7: Time (Hr:Min)
8: Approval status
9: Description
10: Notes
11: Time Off Request
     */
	DATE("Date"), 
	TIMESHEET_NAME("Timesheet name"), 
	CLIENT("Client"), 
	PROJECT("Project"), 
	TIME_TYPE("Time type"),
	USER("User"), 
	HOURS("Time (Hr:Min)"), 
	STATUS("Approval status"), 
	COMMENT("Notes"),
	TIME_OFF_REQUEST("Time Off Request");

	private final String columnName;

	TimeWorkedRecord(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnName() {
		return columnName;
	}

	public static TimeWorkedRecord findByColumnName(String name) {
		for (TimeWorkedRecord value : values()) {
			if (value.columnName.equalsIgnoreCase(name)) {
				return value;
			}
		}
		return null;
	}
}
