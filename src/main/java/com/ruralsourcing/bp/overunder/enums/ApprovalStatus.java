package com.ruralsourcing.bp.overunder.enums;

public enum ApprovalStatus {
    APPROVED,
    SUBMITTED,
    OPEN,
    REJECTED,
    UNKNOWN;

    public static ApprovalStatus findByName(String name) {
        for (ApprovalStatus value : values()) {
            if (value.name().equalsIgnoreCase(name)) {
                return value;
            }
        }
        return UNKNOWN;
    }
}
