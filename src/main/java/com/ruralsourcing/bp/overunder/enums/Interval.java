package com.ruralsourcing.bp.overunder.enums;

/**
 * Represents billing periods for project managers to select and configure
 *
 * @see Bucket
 * @since 1.0
 */
public enum Interval {
  MONTH, QUARTER, SEMIANNUAL, ANNUAL
}
