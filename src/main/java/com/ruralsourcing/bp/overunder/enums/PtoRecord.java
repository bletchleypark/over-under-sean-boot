package com.ruralsourcing.bp.overunder.enums;

public enum PtoRecord {
  
    DATE("Date"),
    USER("User"),
    TASK("Task"),
    HOURS("Hours per day"),
    APPROVAL_STATUS("Approval status"),
    DESCRIPTION("Description"), 
    FILENAME("filename");

    private final String columnName;

    PtoRecord(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public static PtoRecord findByColumnName(String columnName) {
        for (PtoRecord value : values()) {
            if (value.getColumnName().equalsIgnoreCase(columnName)) {
                return value;
            }
        }
        return null;
    }
}
