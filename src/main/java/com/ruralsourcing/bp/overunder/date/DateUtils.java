package com.ruralsourcing.bp.overunder.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.IsoFields;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;
import com.ruralsourcing.bp.overunder.domain.WorkerEntry;
import com.ruralsourcing.bp.overunder.enums.Interval;
import static com.ruralsourcing.bp.overunder.date.QuarterlyBillingUtils.*;
import static com.ruralsourcing.bp.overunder.date.MonthlyBillingUtils.*;

public abstract class DateUtils {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  public static final int Q1 = 1;
  public static final int Q2 = 2;
  public static final int Q3 = 3;
  public static final int Q4 = 4;

  protected static final LocalDate[] RSI_HOLIDAYS = {
          LocalDate.parse("2018-01-01", FORMATTER), LocalDate.parse("2018-05-28", FORMATTER),
          LocalDate.parse("2018-07-04", FORMATTER), LocalDate.parse("2018-09-03", FORMATTER),
          LocalDate.parse("2018-11-22", FORMATTER), LocalDate.parse("2018-11-23", FORMATTER),
          LocalDate.parse("2018-12-25", FORMATTER), LocalDate.parse("2019-01-01", FORMATTER)};

  public static LocalDate getDate(String date) {
    return LocalDate.parse(date, FORMATTER);
  }

  public static boolean isNextDayBeforeCurrentDay(String currentDay, String nextDay) {
    LocalDate current = LocalDate.parse(currentDay, FORMATTER);
    LocalDate next = LocalDate.parse(nextDay, FORMATTER);
    return next.isBefore(current);
  }

  public static boolean isWorkerEntryInCurrentBillingPeriod(WorkerEntry entry,
      Interval billingPeriod) {

    LocalDate entryDate = getDate(entry.getEntryDate().toString());
    String filename = entry.getFilename();
    if (billingPeriod.toString().equals("QUARTER")) {
      return filename == null ? entryDate.get(IsoFields.QUARTER_OF_YEAR) == getCurrentQuarterValue()
          : getCurrentQuarterValueFromFileName(filename) == getCurrentQuarterValue();
    } else if (billingPeriod.toString().equals("MONTH")) {
      return filename == null ? entryDate.getMonthValue() == getCurrentMonthValue() : false;
    } else {
      return false;
    }
  }

  public static Optional<Boolean> isWorkerStartDateAfterStartOfCurrentBillingPeriod(
      LocalDate startDate, Interval billingPeriod) {
    if (billingPeriod.toString().equals("QUARTER")) {
      return Optional.of(startDate.get(IsoFields.QUARTER_OF_YEAR) == getCurrentQuarterValue()
          && startDate.isAfter(firstWorkDayOfCurrentQuarter()));
    } else if (billingPeriod.toString().equals("MONTH")) {
      return Optional.of(startDate.getMonthValue() == getCurrentMonthValue()
          && startDate.isAfter(firstWorkingDayOfMonth()));
    } else {
      return Optional.empty();
    }
  }


  private static int getCurrentQuarterValueFromFileName(String filename) {
    String[] filenameArray = filename.split("_");
    return Integer.parseInt(filenameArray[4].substring(1));
  }

  private static int getCurrentQuarterValue() {
    return LocalDate.now().get(IsoFields.QUARTER_OF_YEAR);
  }

  private static int getCurrentMonthValue() {
    return LocalDate.now().getMonthValue();
  }

  public static int remainingHolidayHours(Interval billingInterval) {
    int holidayHours = 0;
    if (billingInterval.toString().equals("QUARTER")) {
      for (LocalDate holiday : RSI_HOLIDAYS) {
        boolean holidayIsInQuarter =
            !holiday.isBefore(LocalDate.now()) && holiday.isBefore(firstDayOfNextQuarter());
        if (holidayIsInQuarter) {
          holidayHours += 8;
        }
      }
    } else if (billingInterval.toString().equals("MONTH")) {
      for (LocalDate holiday : RSI_HOLIDAYS) {
        Boolean holidayIsInCurrentMonth =
            (!holiday.isBefore(LocalDate.now())) && (holiday.isBefore(lastDayInCurrentMonth()));
        if (holidayIsInCurrentMonth) {
          holidayHours += 8;
        }
      }
    }
    return holidayHours;
  }

  public static String getCurrentMonth() {
    return LocalDate.now().getMonth().toString();
  }

  protected static int getCurrentYear() {
    return LocalDate.now().getYear();
  }

  protected static LocalDate firstDayOfNextQuarter() {
    return LocalDate
        .of(LocalDate.now().getYear(), lastMonthOfCurrentQuarter(), lastDayOfQuarterValue())
        .plusDays(1l);
  }

  public static LocalDate lastDayOfQuarter() {
    return LocalDate.of(LocalDate.now().getYear(), lastMonthOfCurrentQuarter(),
        lastDayOfQuarterValue());
  }

  protected static LocalDate firstWorkDayOfCurrentQuarter() {
    LocalDate firstDayOfCurrentQuarter =
        LocalDate.of(LocalDate.now().getYear(), firstMonthOfCurrentQuarter(), 1);
    while (!isDayAWorkDay(firstDayOfCurrentQuarter)) {
      firstDayOfCurrentQuarter.plusDays(1l);
    }
    return firstDayOfCurrentQuarter;
  }

  protected static Month firstMonthOfCurrentQuarter() {
    return LocalDate.now().getMonth().firstMonthOfQuarter();
  }

  protected static boolean isDayAWorkDay(LocalDate currentDay) {
    return (currentDay.getDayOfWeek() != DayOfWeek.SATURDAY)
        && (currentDay.getDayOfWeek() != DayOfWeek.SUNDAY);

  }

  private static int lastDayOfQuarterValue() {
    return lastMonthOfCurrentQuarter().length(LocalDate.now().isLeapYear());
  }

  protected static Month lastMonthOfCurrentQuarter() {
    return LocalDate.now().getMonth().firstMonthOfQuarter().plus(2);
  }

  protected static LocalDate lastDayInCurrentMonth() {
    return LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
  }

  public static Optional<Integer> workDaysLeftInCurrentBillingPeriod(Interval timeInterval) {
    if (timeInterval.toString().equals("QUARTER")) {
      return Optional.of(workDaysLeftInCurrentQuarter());
    } else if (timeInterval.toString().equals("MONTH")) {
      return Optional.of(1);
    }
    return Optional.empty();
  }

  public static int getProratedWorkDays(LocalDate startDate, Interval billingPeriod) {

    int proratedWorkDays = 0;

    if (billingPeriod.toString().equals("QUARTER")) {

      do {
        if (proratedWorkDays == 0) {
          proratedWorkDays += 1;
          startDate = startDate.minusDays(1l);
          continue;
        }
        if (isDayAWorkDay(startDate))
          proratedWorkDays += 1;
        startDate = startDate.minusDays(1l);
      } while (startDate.isAfter(firstWorkDayOfCurrentQuarter()));

    } else if (billingPeriod.toString().equals("MONTH")) {

      do {
        if (proratedWorkDays == 0) {
          proratedWorkDays += 1;
          startDate = startDate.minusDays(1l);
          continue;
        }
        if (isDayAWorkDay(startDate))
          proratedWorkDays += 1;
        startDate = startDate.minusDays(1l);
      } while (startDate.isAfter(firstWorkingDayOfMonth()));
    }
    return proratedWorkDays;
  }



}
