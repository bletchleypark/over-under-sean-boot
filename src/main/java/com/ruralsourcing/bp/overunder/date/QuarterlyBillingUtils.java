package com.ruralsourcing.bp.overunder.date;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class QuarterlyBillingUtils extends DateUtils {

  /**
   * 
   * @param date the first day of the pay-week
   * @return current quarter of pay-week
   */
  public static int getQuarter(String date) {
    LocalDate workWeekStartDate = getDate(date); 
    return workWeekStartDate.get(IsoFields.QUARTER_OF_YEAR);
  }

  public static int workDaysLeftInCurrentQuarter() {

    // get first day of current work week
    LocalDate currentDay = LocalDate.now().with(DayOfWeek.MONDAY);
    LocalDate firstDayOfNextQuarter = firstDayOfNextQuarter();

    int workDaysLeft = 0;

    while (currentDay.isBefore(firstDayOfNextQuarter)) {
      currentDay = currentDay.plusDays(1l);
      if (isDayAWorkDay(currentDay)) {
        workDaysLeft += 1;
      }
    }
    return workDaysLeft;
  }

  public static int getTotalDaysInQuarter() {

    LocalDate currentDay = LocalDate.of(getCurrentYear(), firstMonthOfCurrentQuarter(), 1);
    LocalDate firstDayOfNextQuarter = firstDayOfNextQuarter();

    int daysInQuarter = 0;

    while (currentDay.isBefore(firstDayOfNextQuarter)
        || currentDay.isEqual(firstDayOfNextQuarter)) {

      currentDay = currentDay.plusDays(1l);
      if (isDayAWorkDay(currentDay)) {
        daysInQuarter += 1;
      }
    }
    return daysInQuarter;
  }

  public static Map<String, Integer> calculateDayDifferentialsForCurrentQuarter(
      Map<String, String> firstWorkerWeekForEachWorker) {

    Map<String, Integer> dayDiffs = new ConcurrentHashMap<>();
    LocalDate startOfQuarter = firstWorkDayOfCurrentQuarter();

    firstWorkerWeekForEachWorker.forEach((worker, firstDay) -> {
      LocalDate currentDay = getDate(firstDay);
      Integer dayDifferential = 0;
      startOfQuarter.plusDays(1l);

      if (currentDay.isEqual(startOfQuarter)) {
        dayDiffs.put(worker, dayDifferential);
      } else if (currentDay.isAfter(startOfQuarter)) {
        while (currentDay.isAfter(startOfQuarter)) {
          currentDay = currentDay.minusDays(1l);
          if (isDayAWorkDay(currentDay)) {
            dayDifferential += 1;
          }
        }
        dayDiffs.put(worker, dayDifferential);
      }
    });
    return dayDiffs;
  }

}
