package com.ruralsourcing.bp.overunder.date;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.Map;
// import static com.ruralsourcing.bp.overunder.date.DateUtils.*;

public class MonthlyBillingUtils extends DateUtils {

  /**
   * This handles the fact that a pay week at RSI could start in one month but be billed in another.
   * 
   * Used for contracts with monthly billing.
   * 
   * @param date the first day of the pay-week
   * @return whether or not the record is in the current pay-month
   */
  public static boolean isDayInCurrentMonth(String day) {

    // get first calendar day of month
    YearMonth ym = YearMonth.now(ZoneId.of("America/New_York"));
    LocalDate monthStart = ym.atDay(1);
    LocalDate monthStop = ym.atEndOfMonth();
    LocalDate currentDay = getDate(day);

    return (!currentDay.isBefore(monthStart)) && (currentDay.isBefore(monthStop));

  }
  
  public static LocalDate firstWorkingDayOfMonth() {
    YearMonth ym = YearMonth.now(ZoneId.of("America/New_York"));
    LocalDate monthStart = ym.atDay(1);
    while (!isDayAWorkDay(monthStart)) {
      monthStart.plusDays(1l);
    }
    return monthStart;
  }


  public static Map<String, Integer> calculateWeekDifferentialForMonth(
      Map<String, String> firstWorkerWeekForEachWorker) {
    return null;
  }
}
