package com.ruralsourcing.bp.overunder.security;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import com.auth0.jwt.JWT;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import static com.ruralsourcing.bp.overunder.security.SecurityConstants.*;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  @Value("${app.token.secret}")
  private String SECRET;

  @Override
  public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res,
      Authentication auth) throws IOException, ServletException {

    String token = JWT.create().withSubject(((User) auth.getPrincipal()).getUsername())
        .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
        .sign(HMAC512(SECRET.getBytes()));

    String json = new Gson().toJson(ImmutableMap.of("token", token));
    res.setContentType("application/json");
    res.setCharacterEncoding("UTF-8");
    res.getWriter().print(json);
    res.getWriter().flush();
  }
}
