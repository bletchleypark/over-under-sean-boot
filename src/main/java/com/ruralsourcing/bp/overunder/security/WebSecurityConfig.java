package com.ruralsourcing.bp.overunder.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.security.jwt.JWTAuthenticationFilter;
import com.ruralsourcing.bp.overunder.security.jwt.JWTAuthorizationFilter;
import com.ruralsourcing.bp.overunder.security.jwt.JwtAuthenticationEntryPoint;
import com.ruralsourcing.bp.overunder.user.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${app.token.secret}")
  @Autowired
  private String SECRET;
  private final CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
  private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
  private final UserDetailsServiceImpl userDetailsService;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;
  private final CustomLogoutSuccessHandler customLogoutHandler;

  public WebSecurityConfig(CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler,
      JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint,
      UserDetailsServiceImpl userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder,
      CustomLogoutSuccessHandler customLogoutSuccessHandler) {

    this.customAuthenticationSuccessHandler = customAuthenticationSuccessHandler;
    this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
    this.userDetailsService = userDetailsService;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    this.customLogoutHandler = customLogoutSuccessHandler;
  }

  public JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {

    JWTAuthenticationFilter jwtAuthenticationFilter =
        new JWTAuthenticationFilter(authenticationManager());
    jwtAuthenticationFilter.setFilterProcessesUrl("/api/v1/auth/login");
    jwtAuthenticationFilter.setAuthenticationSuccessHandler(customAuthenticationSuccessHandler);
    return jwtAuthenticationFilter;
  }

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    // JWTs aren't subject to CSRF
    http.csrf().disable();

    http.cors().and().authorizeRequests()
        // Set path permissions
        .antMatchers(HttpMethod.POST, "api/v1/auth/login").permitAll()
        .antMatchers(HttpMethod.GET, "/api/v1/auth/logout").permitAll()
        .antMatchers(HttpMethod.OPTIONS, "api/v1/users/sign-up").permitAll()
        .antMatchers(HttpMethod.POST, "/api/v1/users/sign-up").permitAll().anyRequest()
        .authenticated().and().exceptionHandling()
        .authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
        .addFilter(jwtAuthenticationFilter())
        .addFilter(new JWTAuthorizationFilter(authenticationManager(), SECRET));

    http.logout().addLogoutHandler(new SecurityContextLogoutHandler())
        .logoutUrl(ApiPaths.LOG_OUT_URL).logoutSuccessHandler(customLogoutHandler)
        .clearAuthentication(true).logoutSuccessUrl("/");

    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(HttpMethod.POST, "api/v1/auth/login").and().ignoring().antMatchers(
        HttpMethod.GET, "/", "/*.html", "/favicon.ico", "/**/*.html", "/**/*.css", "/**/*.js");
  }

}
