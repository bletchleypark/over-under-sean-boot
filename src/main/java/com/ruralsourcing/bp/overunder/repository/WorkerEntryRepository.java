package com.ruralsourcing.bp.overunder.repository;

import com.ruralsourcing.bp.overunder.domain.WorkerEntry;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkerEntryRepository extends JpaRepository<WorkerEntry, Long> {

  List<WorkerEntry> findByClient(String client);
  Optional<List<WorkerEntry>> findByNameAndClientAndUser(String name, String client, String user);
  Optional<List<WorkerEntry>> findByFilename(String filename);
  List<WorkerEntry> findByClientAndProject(String clientName, String projectName);
  List<WorkerEntry> findByFilenameNotNull();
}
