package com.ruralsourcing.bp.overunder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;

public interface BucketPolicyRepository
    extends JpaRepository<BucketPolicy, Long>, CustomBucketPolicyRepository {
}
