package com.ruralsourcing.bp.overunder.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ruralsourcing.bp.overunder.domain.Worker;

public interface WorkerRepository extends JpaRepository<Worker, Long>{
  Optional<Worker> findOneByName(String worker);
}
