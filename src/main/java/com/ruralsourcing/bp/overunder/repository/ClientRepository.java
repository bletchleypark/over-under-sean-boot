package com.ruralsourcing.bp.overunder.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ruralsourcing.bp.overunder.domain.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{

  Optional<Client> findOneByName(String client);
	
}
