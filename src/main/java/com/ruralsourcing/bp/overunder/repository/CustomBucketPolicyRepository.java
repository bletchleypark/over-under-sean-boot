package com.ruralsourcing.bp.overunder.repository;

import java.util.Optional;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.BucketPolicyIdentity;

interface CustomBucketPolicyRepository {
  Optional<BucketPolicy> findById(BucketPolicyIdentity bucketPolicyIdentity);
}
