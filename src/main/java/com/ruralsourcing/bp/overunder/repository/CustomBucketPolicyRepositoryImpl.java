package com.ruralsourcing.bp.overunder.repository;

import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.BucketPolicyIdentity;

public class CustomBucketPolicyRepositoryImpl implements CustomBucketPolicyRepository {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public Optional<BucketPolicy> findById(BucketPolicyIdentity bucketPolicyIdentity) {
    BucketPolicy bucketPolicy = entityManager.find(BucketPolicy.class, bucketPolicyIdentity);
    if (bucketPolicy != null) {
      return Optional.of(entityManager.find(BucketPolicy.class, bucketPolicyIdentity));
    } else {
      return Optional.empty();
    }
  }

}
