package com.ruralsourcing.bp.overunder.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ruralsourcing.bp.overunder.domain.ApplicationUser;

public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
  ApplicationUser findByUsername(String username);
}
