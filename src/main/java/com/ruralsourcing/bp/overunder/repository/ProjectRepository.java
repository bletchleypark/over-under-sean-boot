package com.ruralsourcing.bp.overunder.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.ruralsourcing.bp.overunder.domain.Project;

public interface ProjectRepository extends JpaRepository<Project, Long> {
  Optional<Project> findOneByName(String project);
  List<Project> findByClientId(Long clientId);
}
