package com.ruralsourcing.bp.overunder.utils;

import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.Project;
import static com.ruralsourcing.bp.overunder.date.DateUtils.*;
import java.io.Serializable;
import java.time.LocalDate;

public class OverUnderCalculator {

  private final WorkerInfo workerInfo;
  private final Project project;
  private final BucketPolicy bucketPolicy;
  private final double remainingHolidayDayHours;
  private final double multiplier;
  private double remainingWorkDayHours;
  private Double requiredHours;
  private Double thresholdOver;
  private Double thresholdUnder;
  private Double projectedHours = 0d;

  public OverUnderCalculator(Project project, BucketPolicy bucketPolicy, int minutesLogged,
      String worker) {

    this.project = project;
    this.bucketPolicy = bucketPolicy;
    multiplier = bucketPolicy.getMultiplier().doubleValue();

    workerInfo = new WorkerInfo(worker);

    remainingHolidayDayHours = remainingHolidayHours(project.getTimeInterval());
    remainingWorkDayHours = workDaysLeftInCurrentBillingPeriod(project.getTimeInterval()).get() * 8;

    requiredHours = (double) project.getHours();
    thresholdOver = (double) project.getThresholdOver();
    thresholdUnder = (double) project.getThresholdUnder();

    if (bucketPolicy.getMultiplier().intValue() != 1) {
      requiredHours = requiredHours - (requiredHours * multiplier);
      thresholdOver = thresholdOver - (thresholdOver * multiplier);
      thresholdUnder = thresholdUnder - (thresholdUnder * multiplier);
      remainingWorkDayHours = remainingWorkDayHours - (remainingWorkDayHours * multiplier);
    }
    projectedHours = (projectedHours - remainingHolidayDayHours) + remainingWorkDayHours;
    projectedHours = (minutesLogged / 60) + projectedHours;
  }

  public WorkerInfo getWorkerInfo() {

    LocalDate workerStartDate = bucketPolicy.getStartDate();
    double hoursProjected = projectedHours.intValue();

    boolean isWorkerStartDateAfterCurrentBillingPeriodStart =
        isWorkerStartDateAfterStartOfCurrentBillingPeriod(workerStartDate,
            project.getTimeInterval()).get();

    if (isWorkerStartDateAfterCurrentBillingPeriodStart) {
      int proratedWorkDays = getProratedWorkDays(workerStartDate, project.getTimeInterval());

      if (bucketPolicy.getMultiplier().intValue() != 1) {
        hoursProjected = proratedWorkDays * ((8 * multiplier));
      } else {
        hoursProjected = hoursProjected + (proratedWorkDays * 8);
      }
    }
    if (hoursProjected >= (thresholdOver)) {
      workerInfo.setOver(true);
      workerInfo.setAmount((hoursProjected) - requiredHours);
    } else if (hoursProjected <= thresholdUnder) {
      workerInfo.setUnder(true);
      workerInfo.setAmount((requiredHours - hoursProjected));
      workerInfo.setActionItem(thresholdUnder - hoursProjected);
    } else {
      workerInfo.setWithinThreshold(true);
      workerInfo.setAmount((hoursProjected - requiredHours));
      if (hoursProjected < requiredHours) {
        workerInfo.setActionItem(hoursProjected - thresholdUnder);
      }
    }
    return workerInfo;

  }

  public class WorkerInfo implements Serializable {

    private static final long serialVersionUID = -6204238011788261928L;
    private final String worker;
    private Double amount;
    private Double actionItem;
    private boolean isOver = false;
    private boolean isUnder = false;
    private boolean isWithinThreshold = false;

    WorkerInfo(String worker) {
      this.worker = worker;
    }

    void setAmount(Double amount) {
      this.amount = amount;
    }

    void setActionItem(Double actionItem) {
      this.actionItem = actionItem;
    }

    void setOver(boolean isOver) {
      this.isOver = isOver;
    }

    void setUnder(boolean isUnder) {
      this.isUnder = isUnder;
    }

    void setWithinThreshold(boolean isWithinThreshold) {
      this.isWithinThreshold = isWithinThreshold;
    }

  }

}
