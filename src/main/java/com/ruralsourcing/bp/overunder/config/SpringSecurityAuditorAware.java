package com.ruralsourcing.bp.overunder.config;

import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SpringSecurityAuditorAware implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
      Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (auth != null) {
        Optional<String> opt = Optional.of(auth.getPrincipal().toString());
        return opt;
      }
      return null;
  }
  
}
