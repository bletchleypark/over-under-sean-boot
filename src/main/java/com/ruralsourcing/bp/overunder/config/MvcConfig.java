package com.ruralsourcing.bp.overunder.config;

import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import java.util.concurrent.TimeUnit;

@EnableWebMvc
public class MvcConfig implements WebMvcConfigurer {
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/service-worker.js").addResourceLocations("/public/")
        .setCacheControl(CacheControl.noStore()).setCacheControl(CacheControl.noCache())
        .setCacheControl(CacheControl.maxAge(0, TimeUnit.SECONDS));
  }
}
