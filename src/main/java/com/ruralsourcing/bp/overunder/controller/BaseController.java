package com.ruralsourcing.bp.overunder.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import com.google.common.collect.ImmutableMap;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.dto.ErrorDto;

@RestController
@RequestMapping(ApiPaths.API_ROOT)
public abstract class BaseController {
  
  ResponseEntity<?> generateError(RuntimeException e) {
    e.printStackTrace();
    return new ResponseEntity<>(ImmutableMap.of("error", new ErrorDto(e.getMessage())),
        HttpStatus.BAD_REQUEST);
  }

  ResponseEntity<?> generateError(IOException e) {
    e.printStackTrace();
    return new ResponseEntity<>(ImmutableMap.of("error", new ErrorDto(e.getMessage())),
        HttpStatus.BAD_REQUEST);
  }
}
