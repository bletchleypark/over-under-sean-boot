package com.ruralsourcing.bp.overunder.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.common.collect.ImmutableMap;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.api.services.ProjectService;
import com.ruralsourcing.bp.overunder.domain.Project;

@Controller
public class ProjectController extends BaseController {
  
  private final ProjectService projectService;
  
  public ProjectController(ProjectService projectService) {
    this.projectService = projectService;
  }

  @GetMapping(ApiPaths.PROJECTS)
  public ResponseEntity<?> findAll() {
    try {
      List<Project> projects = projectService.findAll();
      return ResponseEntity.ok(ImmutableMap.of("projects", projects));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }
  
  @GetMapping(ApiPaths.PROJECTS_BY_CLIENTID)
  public ResponseEntity<?> findByClientId(@PathVariable("clientId") Long clientId) {
    try {
      List<Project> projects = projectService.findByClientId(clientId);
      return ResponseEntity.ok(ImmutableMap.of("projects", projects));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }
  @PutMapping(ApiPaths.PROJECTS)
  public ResponseEntity<?> update(@RequestBody Project project) {
    try {
      Project updatedProject = projectService.update(project);
      return ResponseEntity.ok(ImmutableMap.of("project", updatedProject));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }
  
}
