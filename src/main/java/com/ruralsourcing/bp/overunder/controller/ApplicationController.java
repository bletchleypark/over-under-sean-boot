package com.ruralsourcing.bp.overunder.controller;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.api.services.ApplicationService;
import com.ruralsourcing.bp.overunder.domain.ApplicationUser;
import com.ruralsourcing.bp.overunder.utils.OverUnderCalculator.WorkerInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

/**
 * ApplicationController
 */
@Component
public class ApplicationController extends BaseController {

  private final ApplicationService applicationService;

  ApplicationController(ApplicationService applicationService) {
    this.applicationService = applicationService;
  }

  @GetMapping(ApiPaths.AUTHORIZE)
  public ResponseEntity<ImmutableMap<String, Boolean>> authorize() {
    return new ResponseEntity<>(ImmutableMap.of("isLoggedIn", true), HttpStatus.OK);
  }

  @GetMapping(ApiPaths.OVER_UNDER)
  public ResponseEntity<?> getProjectedHoursByClientAndProject(
      @PathVariable("clientName") String clientName,
      @PathVariable("projectName") String projectName) {

    try {
      List<WorkerInfo> workersWithProjectedHours =
          applicationService.getProjectedHours(clientName, projectName);
      return ResponseEntity
          .ok(new Gson().toJson(ImmutableMap.of("workers", workersWithProjectedHours)));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }

  @PostMapping(ApiPaths.SIGN_UP)
  public ResponseEntity<?> signUp(@RequestBody ApplicationUser user) {
    try {
      String username = applicationService.signUp(user);
      return ResponseEntity.ok(ImmutableMap.of("user", username));
    } catch (RuntimeException e) {
      return generateError(e);
    }

  }

  @PostMapping(ApiPaths.UPLOAD_WORK)
  public ResponseEntity<?> handleCsvUpload(@RequestParam("file") MultipartFile file,
      @RequestParam("filename") String filename) {
    try {
      applicationService.handleCsvUpload(file, filename);
      return ResponseEntity.ok(ImmutableMap.of("success", true));
    } catch (IOException e) {
      return generateError(e);
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }

  @PostMapping(ApiPaths.UPLOAD_PTO)
  public ResponseEntity<?> handleCsvPtoUpload(@RequestParam("file") MultipartFile file,
      @RequestParam("filename") String filename) {
    try {
      applicationService.handleCsvPtoUpload(file, filename);
      return ResponseEntity.ok(ImmutableMap.of("success", true));
    } catch (IOException e) {
      return generateError(e);
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }
}
