package com.ruralsourcing.bp.overunder.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.common.collect.ImmutableMap;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.api.services.BucketService;
import com.ruralsourcing.bp.overunder.api.services.ClientService;
import com.ruralsourcing.bp.overunder.api.services.ProjectService;
import com.ruralsourcing.bp.overunder.api.services.WorkerService;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.Client;
import com.ruralsourcing.bp.overunder.domain.Project;
import com.ruralsourcing.bp.overunder.domain.Worker;

@Component
public class BucketController extends BaseController {

  private final BucketService bucketService;
  private final ClientService clientService;
  private final ProjectService projectService;
  private final WorkerService workerService;

  public BucketController(BucketService bucketService, ClientService clientService,
      ProjectService projectService, WorkerService workerService) {
    this.bucketService = bucketService;
    this.clientService = clientService;
    this.projectService = projectService;
    this.workerService = workerService;
  }

  @GetMapping(ApiPaths.BUCKET_FROM_CLIENT_PROJET_WORKER)
  public ResponseEntity<?> findByCompositeKey(@PathVariable() Long clientId,
      @PathVariable("projectId") Long projectId, @PathVariable("workerId") Long workerId) {

    try {
      Client client = clientService.get(clientId);
      Project project = projectService.get(projectId);
      Worker worker = workerService.get(workerId);
      Optional<BucketPolicy> bucket = bucketService.find(client, project, worker);
      if (bucket.isPresent()) {
        return ResponseEntity.ok(ImmutableMap.of("bucket", bucket));
      } else {
        return ResponseEntity.ok(ImmutableMap.of("bucket", ""));

      }
    } catch (RuntimeException e) {
      return generateError(e);
    }

  }

  @GetMapping(ApiPaths.BUCKET_FROM_CLIENT_PROJET)
  public ResponseEntity<?> findByCompositeKey(@PathVariable("clientId") Long clientId,
      @PathVariable("projectId") Long projectId) {

    try {
      Client client = clientService.get(clientId);
      Project project = projectService.get(projectId);
      List<BucketPolicy> buckets = bucketService.findByClientAndProject(client, project);
      if (buckets.size() > 0) {
        return ResponseEntity.ok(ImmutableMap.of("buckets", buckets));
      } else {
        return ResponseEntity.ok(ImmutableMap.of("buckets", new ArrayList<>()));

      }
    } catch (RuntimeException e) {
      return generateError(e);
    }

  }

  @PutMapping(ApiPaths.BUCKET)
  public ResponseEntity<?> update(@RequestBody BucketPolicy bucketPolicy) {
    try {
      BucketPolicy bucket = bucketService.update(bucketPolicy);
      return ResponseEntity.ok(ImmutableMap.of("bucket", bucket));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }

}
