package com.ruralsourcing.bp.overunder.controller;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import com.google.common.collect.ImmutableMap;
import com.ruralsourcing.bp.overunder.api.ApiPaths;
import com.ruralsourcing.bp.overunder.api.services.ClientService;
import com.ruralsourcing.bp.overunder.domain.Client;

@Component
public class ClientController extends BaseController {
  
  private final ClientService clientService;
  
  public ClientController(ClientService clientService) {
    this.clientService = clientService;
  }
  
  @GetMapping(ApiPaths.CLIENT)
  public ResponseEntity<?> getAllClients() {
    try {
      List<Client> clients = clientService.getAllClients();
      return ResponseEntity.ok(ImmutableMap.of("clients", clients));
    } catch (RuntimeException e) {
      return generateError(e);
    }
  }
  
}
