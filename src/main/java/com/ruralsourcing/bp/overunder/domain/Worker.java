package com.ruralsourcing.bp.overunder.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Worker extends BaseDomainObject {
  
  @Column(unique = true)
  private String name;

  public Worker() {
  }
  
  public Worker(String name) {
    this.name = name;
  }
  
  
  
}
