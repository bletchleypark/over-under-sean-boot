package com.ruralsourcing.bp.overunder.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Size;

@Entity
public class Client extends BaseDomainObject {
  
  @Size(min = 1, max = 240)
  @Column(unique = true)
  private String name;
  
  public Client() {}
  
  public Client(String name) {
    this.name = name;
  } 

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
