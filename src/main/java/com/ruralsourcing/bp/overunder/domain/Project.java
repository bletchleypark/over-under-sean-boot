package com.ruralsourcing.bp.overunder.domain;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import com.ruralsourcing.bp.overunder.enums.Interval;

@Entity
public class Project extends BaseDomainObject {
  
  @Column(unique = true)
  private String name;
  private LocalDate startDate; 
  private LocalDate endDate; 
  @ManyToOne
  private Client client; 
  
  @Enumerated(EnumType.STRING)
  private Interval timeInterval;
  
  private Integer thresholdUnder; 
  private Integer thresholdOver; 
  private Integer hours; 
  
  public Project() {
  } 
  

  public Project(String name, Client client) {
    this.name = name;
    this.client = client;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Interval getTimeInterval() {
    return timeInterval;
  }

  public void setTimeInterval(Interval timeInterval) {
    this.timeInterval = timeInterval;
  }

  public Integer getThresholdUnder() {
    return thresholdUnder;
  }

  public void setThresholdUnder(Integer thresholdUnder) {
    this.thresholdUnder = thresholdUnder;
  }

  public Integer getThresholdOver() {
    return thresholdOver;
  }

  public void setThresholdOver(Integer thresholdOver) {
    this.thresholdOver = thresholdOver;
  }

  public Integer getHours() {
    return hours;
  }

  public void setHours(Integer hours) {
    this.hours = hours;
  } 
  
}
