package com.ruralsourcing.bp.overunder.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
  
@Entity
public class BucketPolicy {
  
  @EmbeddedId
  private BucketPolicyIdentity bucketPolicyIdentity;
  @NotNull
  private String name;
  private LocalDate startDate;
  @Column(precision=10, scale=2)
  private BigDecimal multiplier;
  
  public BucketPolicy() {}
  
  public BucketPolicy(BucketPolicyIdentity bucketPolicyIdentity, @NotNull String name,
      LocalDate startDate, BigDecimal multiplier) {
    super();
    this.bucketPolicyIdentity = bucketPolicyIdentity;
    this.name = name;
    this.startDate = startDate;
    this.multiplier = multiplier;
  }

  public BucketPolicyIdentity getBucketPolicyIdentity() {
    return bucketPolicyIdentity;
  }

  public void setBucketPolicyIdentity(BucketPolicyIdentity bucketPolicyIdentity) {
    this.bucketPolicyIdentity = bucketPolicyIdentity;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public BigDecimal getMultiplier() {
    return multiplier;
  }

  public void setMultiplier(BigDecimal multiplier) {
    this.multiplier = multiplier;
  }
  
}
