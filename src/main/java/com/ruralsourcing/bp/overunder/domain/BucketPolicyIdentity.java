package com.ruralsourcing.bp.overunder.domain;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Embeddable
public class BucketPolicyIdentity implements Serializable {

  @NotNull
  @OneToOne
  private Client client;

  @NotNull
  @OneToOne
  private Project project;

  @NotNull
  @OneToOne
  private Worker worker;

  public BucketPolicyIdentity() {}
  
  public BucketPolicyIdentity(@NotNull Client client, @NotNull Project project,
      @NotNull Worker worker) {
    this.client = client;
    this.project = project;
    this.worker = worker;
  }
  
  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }
  
  public Worker getWorker() {
    return worker;
  }
  
  public void setWorker(Worker worker) {
    this.worker = worker;
  }
  
}
