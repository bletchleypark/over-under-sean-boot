package com.ruralsourcing.bp.overunder.domain;

import com.ruralsourcing.bp.overunder.enums.ApprovalStatus;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(indexes = {@Index(name = "IDX_MYIDX1", columnList = "name")})
public class WorkerEntry extends BaseDomainObject {

  private String filename;
  @Temporal(TemporalType.DATE)
  private Date entryDate;
  private String name;
  private String client;
  private String project;
  private String timeType;
  private String user;
  private Integer minutes;
  @Enumerated(EnumType.STRING)
  private ApprovalStatus status;
  @Column(length = 100_000)
  private String note;
  private String timeOffRequest;
  private boolean pto;
  
  public WorkerEntry() {}
  
  public WorkerEntry(
      String filename, 
      Date date, 
      String timeType, 
      String user, 
      Integer minutes, 
      ApprovalStatus status, 
      String description) {
    this.filename = filename;
    entryDate = date;
    this.timeType = timeType;
    this.user = user;
    this.minutes = minutes;
    this.status = status;
    this.note = description;
  }

  public WorkerEntry(
      Date entryDate, 
      String name, 
      String client,
      String project, 
      String timeType, 
      String user, 
      Integer minutes, 
      ApprovalStatus status,
      String note, 
      String timeOffRequest, 
      boolean pto) {
    this.entryDate = entryDate;
    this.name = name;
    this.client = client;
    this.project = project;
    this.timeType = timeType;
    this.user = user;
    this.minutes = minutes;
    this.status = status;
    this.note = note;
    this.timeOffRequest = timeOffRequest;
    this.pto = pto;
  }

  public String getFilename() {
    return filename;
  }

  public Date getEntryDate() {
    return entryDate;
  }

  public String getName() {
    return name;
  }

  public String getClient() {
    return client;
  }

  public String getProject() {
    return project;
  }

  public String getTimeType() {
    return timeType;
  }

  public String getUser() {
    return user;
  }

  public Integer getMinutes() {
    return minutes;
  }

  public ApprovalStatus getStatus() {
    return status;
  }

  public String getNote() {
    return note;
  }

  public String getTimeOffRequest() {
    return timeOffRequest;
  }

  public boolean isPto() {
    return pto;
  }
  
}
