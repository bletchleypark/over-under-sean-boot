package com.ruralsourcing.bp.overunder.api;

public class ApiPaths {

  // Root
  public static final String API_ROOT = "api/v1/";

  // Projected Hours
  public static final String OVER_UNDER =
      "projectedhours/client/{clientName}/project/{projectName}";

  // Bucket
  public static final String BUCKET = "/buckets";
  public static final String BUCKET_FROM_CLIENT_PROJET_WORKER =
      "/buckets/{clientId}/{projectId}/{workerId}";
  public static final String BUCKET_FROM_CLIENT_PROJET =
      "/buckets/client/{clientId}/project/{projectId}";

  // Project
  public static final String PROJECTS = "/projects";
  public static final String PROJECTS_BY_CLIENTID = "projects/client/{clientId}";

  // Client
  public static final String CLIENT = "/clients";

  // Authentication for
  public static final String AUTHORIZE = "/authorize";
  public static final String SIGN_UP = "/users/sign-up";

  // Authentication (full path for config file)
  public static final String FILTER_PROCESSING_URL = "api/v1/auth/login";
  public static final String SIGN_UP_ANTMATCHER = "api/v1/users/sign-up";
  public static final String LOGIN_ANTMATCHER = "api/v1/auth/login";
  public static final String LOG_OUT_ANTMATCHER = "api/v1/auth/logout";
  public static final String LOG_OUT_URL = "/api/v1/auth/logout";

  // CSV Parsing
  public static final String UPLOAD_PTO = "/uploadpto";
  public static final String UPLOAD_WORK = "/uploadwork";


}
