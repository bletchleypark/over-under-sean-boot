package com.ruralsourcing.bp.overunder.api.services;

import static com.ruralsourcing.bp.overunder.date.DateUtils.getCurrentMonth;
import static com.ruralsourcing.bp.overunder.date.MonthlyBillingUtils.isDayInCurrentMonth;
import static com.ruralsourcing.bp.overunder.date.QuarterlyBillingUtils.getQuarter;
import java.time.LocalDate;
import java.time.temporal.IsoFields;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.BucketPolicyIdentity;
import com.ruralsourcing.bp.overunder.domain.Client;
import com.ruralsourcing.bp.overunder.domain.Project;
import com.ruralsourcing.bp.overunder.domain.Worker;
import com.ruralsourcing.bp.overunder.domain.WorkerEntry;
import com.ruralsourcing.bp.overunder.enums.ApprovalStatus;
import com.ruralsourcing.bp.overunder.repository.BucketPolicyRepository;
import com.ruralsourcing.bp.overunder.repository.ProjectRepository;
import com.ruralsourcing.bp.overunder.repository.WorkerEntryRepository;

@Service
public class BucketService {

  private final BucketPolicyRepository bucketRepository;
  private final ProjectRepository projectRepository;
  private final WorkerEntryRepository workerEntryRepository;

  public BucketService(BucketPolicyRepository bucketRepository, ProjectRepository projectRepository,
      WorkerEntryRepository workerEntryRepository) {
    this.bucketRepository = bucketRepository;
    this.projectRepository = projectRepository;
    this.workerEntryRepository = workerEntryRepository;
  }

  public Optional<BucketPolicy> find(Client client, Project project, Worker worker)
      throws RuntimeException {
    Optional<BucketPolicy> bucket =
        bucketRepository.findById(new BucketPolicyIdentity(client, project, worker));
    return bucket;
  }

  public Map<String, Map<String, Integer>> findByProjectName(String clientname)
      throws RuntimeException {

    Project project = projectRepository.findOneByName(clientname).get();
    List<WorkerEntry> entries = workerEntryRepository.findByClient(clientname);

    Map<String, Map<String, Integer>> hoursWorkedByDate = new ConcurrentHashMap<>();
    Map<String, Integer> workers = new ConcurrentHashMap<>();

    entries.forEach(entry -> {
      if (entry.getStatus() != ApprovalStatus.REJECTED) {

        if (project.getTimeInterval().toString().equalsIgnoreCase("QUARTER")) {

          int entryQuarter = getQuarter(entry.getEntryDate().toString());
          int currentQuarter = LocalDate.now().get(IsoFields.QUARTER_OF_YEAR);

          if (entryQuarter == currentQuarter) {

            String user = entry.getUser();
            int totalMin = entry.getMinutes();

            if (workers.containsKey(user)) {
              int currentTotalMin = workers.get(user);
              totalMin = currentTotalMin + totalMin;
              workers.put(user, totalMin);
            } else {
              workers.put(user, totalMin);
            }
            hoursWorkedByDate.put(getQuarterStringValueForMap(entryQuarter), workers);
          }
        } else if (project.getTimeInterval().toString().equalsIgnoreCase("MONTH")) {

          boolean isDayInMonth = isDayInCurrentMonth(entry.getEntryDate().toString());

          if (isDayInMonth) {

            String user = entry.getUser();
            int totalMin = entry.getMinutes();

            if (workers.containsKey(user)) {
              totalMin = workers.get(user) + totalMin;
              workers.put(user, totalMin);
            } else {
              workers.put(user, totalMin);
            }

            hoursWorkedByDate.put(getCurrentMonth(), workers);
          }
        }
      }
    });

    return hoursWorkedByDate;
  }

  public void create(BucketPolicy bucketPolicy) {
    bucketRepository.save(bucketPolicy);
  }

  private String getQuarterStringValueForMap(int quarter) {
    switch (quarter) {
      case 1:
        return "Q1";
      case 2:
        return "Q2";
      case 3:
        return "Q3";
      case 4:
        return "Q4";
      default:
        return "";
    }
  }

  public List<BucketPolicy> findByClientAndProject(Client client, Project project) {
    return bucketRepository.findAll().stream()
        .filter(b -> b.getBucketPolicyIdentity().getClient().getId() == client.getId()
            && b.getBucketPolicyIdentity().getProject().getId() == project.getId())
        .collect(Collectors.toList());
  }

  public Optional<BucketPolicy> findById(BucketPolicyIdentity bucketPolicyIdentity) {
    return bucketRepository.findById(bucketPolicyIdentity);
  }

  public BucketPolicy update(BucketPolicy bucketPolicy) {
    return bucketRepository.save(bucketPolicy);
  }

}
