package com.ruralsourcing.bp.overunder.api.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.ruralsourcing.bp.overunder.domain.Worker;
import com.ruralsourcing.bp.overunder.repository.WorkerRepository;

@Service
public class WorkerService {
  
  private final WorkerRepository workerRepository;

  public WorkerService(WorkerRepository workerRepository) {
    this.workerRepository = workerRepository;
  }
  
  public List<Worker> findAll() {
    return workerRepository.findAll();
  }
  
  public Worker get(Long id) {
    return workerRepository.findById(id).get();
  }

  public Optional<Worker> findOneByName(String workerName) {
    return workerRepository.findOneByName(workerName);
  }

  public void save(Worker worker) {
    workerRepository.save(worker);
  }

  public Worker getOrCreateWorkerFromRecord(String workerName) {
    Optional<Worker> duplicateWorker = findOneByName(workerName);
    Worker worker = null;
    if (!duplicateWorker.isPresent()) {
      worker = new Worker(workerName);
      save(worker);
    } else {
      worker = findOneByName(workerName).get();
    }
    return worker;
  }
  
}
