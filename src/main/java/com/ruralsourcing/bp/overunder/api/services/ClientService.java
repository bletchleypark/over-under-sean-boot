package com.ruralsourcing.bp.overunder.api.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.ruralsourcing.bp.overunder.domain.Client;
import com.ruralsourcing.bp.overunder.repository.ClientRepository;

@Service
public class ClientService {

  private final ClientRepository clientRepository;

  public ClientService(ClientRepository clientRepository) {
    this.clientRepository = clientRepository;
  }

  public List<Client> getAllClients() {
    return clientRepository.findAll();
  }

  public Client get(Long id) {
    return clientRepository.findById(id).get();
  }

  public Optional<Client> findOneByName(String clientName) {
    return clientRepository.findOneByName(clientName);
  }

  public void save(Client client) {
    clientRepository.save(client);
  }

  public Optional<Client> findById(Long id) {
    return clientRepository.findById(id);
  }

  public Client getOrCreateClientFromRecord(String clientName) {
    Optional<Client> duplicateClient = findOneByName(clientName);
    Client client = null;
    if (!duplicateClient.isPresent()) {
      client = new Client(clientName);
      save(client);
    } else {
      client = findOneByName(clientName).get();
    }
    return client;
  }

}
