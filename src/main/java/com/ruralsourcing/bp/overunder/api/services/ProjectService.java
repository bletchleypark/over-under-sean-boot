package com.ruralsourcing.bp.overunder.api.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.ruralsourcing.bp.overunder.domain.Project;
import com.ruralsourcing.bp.overunder.domain.Client;
import com.ruralsourcing.bp.overunder.repository.ProjectRepository;

@Service
public class ProjectService {

  private final ProjectRepository projectRepository;

  public ProjectService(ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  public List<Project> findAll() throws RuntimeException {
    return projectRepository.findAll();
  }

  public Project get(Long id) {
    return projectRepository.findById(id).get();
  }

  public List<Project> findByClientId(Long clientId) {
    return projectRepository.findByClientId(clientId);
  }

  public Project update(Project project) {
    // Project project = projectRepository.findById(updateProjectDto.getId()).get();
    // project.setEndDate(DateUtils.getDate(updateProjectDto.getEndDate()));
    // project.setStartDate(DateUtils.getDate(updateProjectDto.getStartDate()));
    // project.setHours(updateProjectDto.getHours());
    // project.setThresholdOver(updateProjectDto.getThresholdOver());
    // project.setThresholdUnder(updateProjectDto.getThresholdUnder());
    // project.setTimeInterval(Interval.valueOf(updateProjectDto.getTimeInterval()));
    return projectRepository.save(project);
  }

  public Optional<Project> findOneByName(String projectname) {
    return projectRepository.findOneByName(projectname);
  }

  public void save(Project project) {
    projectRepository.save(project);
  }

  public Project getOrCreateProjectFromRecord(String projectName, Client client) {
    Optional<Project> duplicateProject = findOneByName(projectName);
    Project project = null;
    if (!duplicateProject.isPresent()) {
      project = new Project(projectName, client);
      save(project);
    } else {
      project = findOneByName(projectName).get();
    }
    return project;
  }

}
