package com.ruralsourcing.bp.overunder.api.services;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import com.ruralsourcing.bp.overunder.domain.WorkerEntry;
import com.ruralsourcing.bp.overunder.repository.WorkerEntryRepository;

@Service
public class WorkerEntryService {

  private final WorkerEntryRepository workerEntryRepository;

 public WorkerEntryService(WorkerEntryRepository workerEntryRepository) {
    this.workerEntryRepository = workerEntryRepository;
  }

  public Optional<List<WorkerEntry>> findByNameAndClientAndUser(String name, String client,
      String user) {
    return workerEntryRepository.findByNameAndClientAndUser(name, client, user);
  }

  public void delete(WorkerEntry duplicate) {
    workerEntryRepository.delete(duplicate);
  }

  public void save(WorkerEntry entry) {
    workerEntryRepository.save(entry);
  }

  public Optional<List<WorkerEntry>> findByFilename(String filename) {
    return workerEntryRepository.findByFilename(filename);
  }

  public List<WorkerEntry> findByClientAndProject(String clientName, String projectName) {
    return workerEntryRepository.findByClientAndProject(clientName, projectName);
  }

  public List<WorkerEntry> findByFilenameNotNull() {
    return workerEntryRepository.findByFilenameNotNull();
  }

}
