package com.ruralsourcing.bp.overunder.api.services;

import com.ruralsourcing.bp.overunder.domain.ApplicationUser;
import com.ruralsourcing.bp.overunder.domain.BucketPolicy;
import com.ruralsourcing.bp.overunder.domain.BucketPolicyIdentity;
import com.ruralsourcing.bp.overunder.domain.Client;
import com.ruralsourcing.bp.overunder.domain.Project;
import com.ruralsourcing.bp.overunder.domain.Worker;
import com.ruralsourcing.bp.overunder.enums.TimeWorkedRecord;
import com.ruralsourcing.bp.overunder.repository.ApplicationUserRepository;
import com.ruralsourcing.bp.overunder.utils.OverUnderCalculator;
import com.ruralsourcing.bp.overunder.utils.OverUnderCalculator.WorkerInfo;
import com.ruralsourcing.bp.overunder.domain.WorkerEntry;
import com.ruralsourcing.bp.overunder.enums.ApprovalStatus;
import com.ruralsourcing.bp.overunder.enums.Interval;
import com.ruralsourcing.bp.overunder.enums.PtoRecord;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import static com.ruralsourcing.bp.overunder.date.DateUtils.*;

/**
 * Handles all RESTful, Stateless calls against the app server API
 *
 * @author sean.kelly
 * @since 1.0
 */
@Service
public class ApplicationService {

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yy");

  private final ApplicationUserRepository applicationUserRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;
  private final WorkerEntryService workerEntryService;
  private final ClientService clientService;
  private final ProjectService projectService;
  private final WorkerService workerService;
  private final BucketService bucketService;

  public ApplicationService(ApplicationUserRepository applicationUserRepository,
      BCryptPasswordEncoder bCryptPasswordEncoder, WorkerEntryService workerEntryService,
      ClientService clientService, ProjectService projectService, WorkerService workerService,
      BucketService bucketService) {
    this.applicationUserRepository = applicationUserRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    this.workerEntryService = workerEntryService;
    this.clientService = clientService;
    this.projectService = projectService;
    this.workerService = workerService;
    this.bucketService = bucketService;
  }

  public String signUp(ApplicationUser user) throws RuntimeException {

    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    ApplicationUser newUser = applicationUserRepository.save(user);
    return newUser.getUsername();
  }

  public void handleCsvUpload(MultipartFile file, String filename)
      throws IOException, RuntimeException {

    CSVParser parser =
        CSVParser.parse(file.getInputStream(), Charset.defaultCharset(), CSVFormat.EXCEL);

    EnumMap<TimeWorkedRecord, Integer> columns = new EnumMap<>(TimeWorkedRecord.class);
    Iterator<CSVRecord> iterator = parser.getRecords().iterator();
    List<EnumMap<TimeWorkedRecord, String>> totalRecords = new ArrayList<>();

    int rowSizeExpectations = 0;

    if (iterator.hasNext()) {
      iterator.next(); // Skip useless header text that is added into
                       // all of this
      CSVRecord header = iterator.next(); // This is the line that
                                          // contains all the headers
                                          // for
                                          // our columns
      for (int i = 0; i < header.size(); i++) {
        // We parse the column names to find the specific ones we
        // want, recording the column index
        // for later use.
        TimeWorkedRecord record = TimeWorkedRecord.findByColumnName(header.get(i));
        if (record != null) { // If there is an enum value that
                              // matches this header name
          columns.put(record, i);
          if (rowSizeExpectations < i) {
            rowSizeExpectations = i;
          }
        }
      }

      while (iterator.hasNext()) {
        header = iterator.next();
        if (header.size() < rowSizeExpectations) {
          continue;
        }
        EnumMap<TimeWorkedRecord, String> recordData = new EnumMap<>(TimeWorkedRecord.class);
        for (TimeWorkedRecord record : columns.keySet()) {
          recordData.put(record, header.get(columns.get(record)));
        }
        totalRecords.add(recordData);
      }
    }

    Set<String> checkedNamesAndClientsAndUsers = new HashSet<>();
    int size = totalRecords.size();
    int offset = size - 2;
    for (int i = 0; i <= offset; i++) {
      EnumMap<TimeWorkedRecord, String> currentRecord = totalRecords.get(i);
      boolean rejected = currentRecord.get(TimeWorkedRecord.STATUS).equals("Rejected");
      boolean isRsiTime = currentRecord.get(TimeWorkedRecord.CLIENT).equals("RSI");
      if (rejected || isRsiTime) {
        continue;
      }
      String client = currentRecord.get(TimeWorkedRecord.CLIENT);
      StringBuffer sb = new StringBuffer();
      String name = currentRecord.get(TimeWorkedRecord.TIMESHEET_NAME);
      String user = currentRecord.get(TimeWorkedRecord.USER);
      String checkedNameAndClientAndUser = sb.append(name).append(client).append(user).toString();

      if (!checkedNamesAndClientsAndUsers.contains(checkedNameAndClientAndUser)) {

        Optional<List<WorkerEntry>> duplicates =
            workerEntryService.findByNameAndClientAndUser(name, client, user);

        if (duplicates.isPresent()) {
          duplicates.get().forEach(workerEntryService::delete);
        }
        checkedNamesAndClientsAndUsers.add(checkedNameAndClientAndUser);
      }

      createWorkEntry(currentRecord);
    }

  }

  private void createWorkEntry(EnumMap<TimeWorkedRecord, String> record) throws RuntimeException {

    // Create or get Worker
    String workerName = record.get(TimeWorkedRecord.USER);
    Worker worker = workerService.getOrCreateWorkerFromRecord(workerName);

    // Create or get Client
    String clientName = record.get(TimeWorkedRecord.CLIENT);
    Client client = clientService.getOrCreateClientFromRecord(clientName);

    // Create or get Project
    String projectName = record.get(TimeWorkedRecord.PROJECT);
    Project project = projectService.getOrCreateProjectFromRecord(projectName, client);

    StringBuffer sb = new StringBuffer();
    String bucketPolicyName = sb.append(workerName).append("_").append(clientName).append("_")
        .append(projectName).toString();

    String hoursToSplit = record.get(TimeWorkedRecord.HOURS);
    String[] time = hoursToSplit.split(":");
    int minutes = Integer.valueOf(time[1]) + (Integer.valueOf(time[0]) * 60);
    ApprovalStatus status = ApprovalStatus.findByName(record.get(TimeWorkedRecord.STATUS));

    Date date = null;

    try {
      date = DATE_FORMAT.parse(record.get(TimeWorkedRecord.DATE));
    } catch (ParseException e) {
      e.printStackTrace();
    }

    // Persist Work_entry
    WorkerEntry entry = new WorkerEntry(date, record.get(TimeWorkedRecord.TIMESHEET_NAME),
        record.get(TimeWorkedRecord.CLIENT), record.get(TimeWorkedRecord.PROJECT),
        record.get(TimeWorkedRecord.TIME_TYPE), record.get(TimeWorkedRecord.USER), minutes, status,
        "", record.get(TimeWorkedRecord.TIME_OFF_REQUEST), false);

    workerEntryService.save(entry);

    // Check if Bucket Policy already exists
    BucketPolicyIdentity bucketPolicyIdentity = new BucketPolicyIdentity(client, project, worker);
    Optional<BucketPolicy> duplicateBucketPolicy = bucketService.findById(bucketPolicyIdentity);

    // Persist New Bucket Policy
    if (!duplicateBucketPolicy.isPresent()) {
      BucketPolicy bucketPolicy = new BucketPolicy(bucketPolicyIdentity, bucketPolicyName,
          getDate("2017-01-01"), BigDecimal.ONE);
      bucketService.create(bucketPolicy);
    }
  }

  public void handleCsvPtoUpload(MultipartFile file, String filename)
      throws IOException, RuntimeException {

    Optional<List<WorkerEntry>> duplicateEntries = workerEntryService.findByFilename(filename);
    if (duplicateEntries.isPresent()) {
      duplicateEntries.get().forEach(workerEntryService::delete);
    }
    CSVParser parser =
        CSVParser.parse(file.getInputStream(), Charset.defaultCharset(), CSVFormat.EXCEL);
    Iterator<CSVRecord> recordIterator = parser.getRecords().iterator();
    EnumMap<PtoRecord, Integer> columns = new EnumMap<>(PtoRecord.class);
    List<EnumMap<PtoRecord, String>> totalRecords = new ArrayList<>();
    int rowSizeExpecations = 0;
    if (recordIterator.hasNext()) {
      recordIterator.next();

      CSVRecord header = recordIterator.next();
      for (int i = 0; i < header.size(); i++) {
        PtoRecord record = PtoRecord.findByColumnName(header.get(i));
        if (record != null) {
          columns.put(record, i);
          if (rowSizeExpecations < i) {
            rowSizeExpecations = i;
          }
        }
      }
    }
    while (recordIterator.hasNext()) {
      CSVRecord record = recordIterator.next();
      if (record.size() < rowSizeExpecations) {
        continue;
      }
      EnumMap<PtoRecord, String> recordData = new EnumMap<>(PtoRecord.class);
      for (PtoRecord ptoRecord : columns.keySet()) {
        recordData.put(ptoRecord, record.get(columns.get(ptoRecord)));
      }
      recordData.put(PtoRecord.FILENAME, filename);
      totalRecords.add(recordData);
    }


    int size = totalRecords.size();
    int offset = size - 2;
    for (int i = 0; i <= offset; i++) {
      EnumMap<PtoRecord, String> currentRecord = totalRecords.get(i);
      ApprovalStatus status =
          ApprovalStatus.findByName(currentRecord.get(PtoRecord.APPROVAL_STATUS));
      if (status.toString().equalsIgnoreCase("Rejected")) {
        continue;
      }
      createPtoEntry(currentRecord);
    }

  }

  private void createPtoEntry(EnumMap<PtoRecord, String> record) {
    ApprovalStatus status = ApprovalStatus.findByName(record.get(PtoRecord.APPROVAL_STATUS));
    String hoursToSplit = record.get(PtoRecord.HOURS);
    String[] time = hoursToSplit.split("\\.");
    int minutes = (Integer.valueOf(time[0]) * 60);
    String description = record.get(PtoRecord.DESCRIPTION);
    String timeType = record.get(PtoRecord.TASK);
    String user = record.get(PtoRecord.USER);
    Date date = null;
    try {
      date = DATE_FORMAT.parse(record.get(PtoRecord.DATE));
    } catch (ParseException ex) {
      ex.printStackTrace();
    }

    // Create or get Worker
    String workerName = record.get(PtoRecord.USER);
    Optional<Worker> duplicateWorker = workerService.findOneByName(workerName);
    Worker worker = null;
    if (!duplicateWorker.isPresent()) {
      worker = new Worker(workerName);
      workerService.save(worker);
    } else {
      worker = workerService.findOneByName(workerName).get();
    }

    WorkerEntry entry = new WorkerEntry(record.get(PtoRecord.FILENAME), date, timeType, user,
        minutes, status, description);
    workerEntryService.save(entry);

  }

  public List<WorkerInfo> getProjectedHours(String clientName, String projectName) {

    Client client = clientService.findOneByName(clientName).get();
    Project project = projectService.findOneByName(projectName).get();

    Interval billingPeriod = project.getTimeInterval();

    // Pto records create workers who may not be tied to a bucket policy
    Map<String, Integer> workerHours = new HashMap<>();

    List<WorkerInfo> workers = new ArrayList<>();

    // Get WorkerEntries that are not PTO requests for current billing period
    List<WorkerEntry> workerEntries =
        workerEntryService.findByClientAndProject(clientName, projectName).stream()
            .filter(entry -> isWorkerEntryInCurrentBillingPeriod(entry, billingPeriod))
            .collect(Collectors.toList());

    workerEntries.forEach(entry -> workerHours.put(entry.getUser(), 0));

    // Get WorkerEntries that are PTO requests
    List<WorkerEntry> workerEntriesWithPtoRequests = workerEntryService.findByFilenameNotNull()
        .stream().filter(entry -> isWorkerEntryInCurrentBillingPeriod(entry, billingPeriod)
            && workerHours.containsKey(entry.getUser()))
        .collect(Collectors.toList());

    workerEntries.addAll(workerEntriesWithPtoRequests);

    Map<String, BucketPolicy> bucketPolicies = new HashMap<>();

    // get all bucket Policies
    workerEntries.forEach(entry -> {
      String user = entry.getUser();
      Worker worker = workerService.findOneByName(user).get();
      BucketPolicyIdentity bucketPolicyIdentity = new BucketPolicyIdentity(client, project, worker);
      BucketPolicy bucketPolicy = bucketService.findById(bucketPolicyIdentity).get();

      if (!bucketPolicies.containsKey(entry.getUser())) {
        bucketPolicies.put(entry.getUser(), bucketPolicy);
      }
    });

    // Constants for overunder calculations
    LocalDate startDate = project.getStartDate();
    LocalDate endDate = project.getEndDate();

    /**
     * If project end date does not fall before the end of the current calendar quarter
     */
    if (endDate.isAfter(lastDayOfQuarter()) || endDate.isEqual(lastDayOfQuarter())) {

      // Add work_entry amounts
      workerEntries.forEach(entry -> {

        String currentWorker = entry.getUser();
        int currentMinutes = entry.getMinutes();

        // time worked
        if (entry.getFilename() == null) {
          int totalMinutes = workerHours.get(currentWorker);
          totalMinutes = totalMinutes + currentMinutes;
          workerHours.put(currentWorker, totalMinutes);
          // pto requests
        } else {
          int totalMinutes = workerHours.get(currentWorker);
          totalMinutes = totalMinutes - currentMinutes;
          workerHours.put(currentWorker, totalMinutes);
        }
      });

      workerHours.forEach((worker, minutes) -> {
        OverUnderCalculator overUnderCalculator =
            new OverUnderCalculator(project, bucketPolicies.get(worker), minutes, worker);

        WorkerInfo workerInfo = overUnderCalculator.getWorkerInfo();

        workers.add(workerInfo);

      });

    /**
     * If project end date does indeed fall before the end of the current calendar quarter
     */
    } else if (endDate.isBefore(lastDayOfQuarter())) {
      // Add work_entry amounts
      workerEntries.forEach(entry -> {
        int proratedWorkDays = workDaysLeftInCurrentBillingPeriod(project.getTimeInterval()).get();
        int proratedMinutes = ((proratedWorkDays * 8) * 60);
        String currentWorker = entry.getUser();
        int currentMinutes = entry.getMinutes() + proratedMinutes;

        // time worked
        if (entry.getFilename() == null) {
          int totalMinutes = workerHours.get(currentWorker);
          totalMinutes = totalMinutes + currentMinutes;
          workerHours.put(currentWorker, totalMinutes);
          // pto requests
        } else {
          int totalMinutes = workerHours.get(currentWorker);
          totalMinutes = totalMinutes - currentMinutes;
          workerHours.put(currentWorker, totalMinutes);
        }
      });

      workerHours.forEach((worker, minutes) -> {
        OverUnderCalculator overUnderCalculator =
            new OverUnderCalculator(project, bucketPolicies.get(worker), minutes, worker);

        WorkerInfo workerInfo = overUnderCalculator.getWorkerInfo();

        workers.add(workerInfo);

      });
      
    }
    return workers;

  }

}
